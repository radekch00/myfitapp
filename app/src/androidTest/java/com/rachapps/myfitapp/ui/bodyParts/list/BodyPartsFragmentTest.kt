package com.rachapps.myfitapp.ui.bodyParts.list

import androidx.appcompat.view.ContextThemeWrapper
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify


@MediumTest
@HiltAndroidTest
@ExperimentalCoroutinesApi
class BodyPartsFragmentTest{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp(){
        hiltRule.inject()
    }

    @Test
    fun clickOnAddBodyPartButton_navigateToAddBodyPartFragment(){
        val navController = mock(NavController::class.java)
        launchFragmentInHiltContainer<BodyPartsFragment> {
            Navigation.setViewNavController(requireView(), navController)
        }

        onView(withId(R.id.fab_add_body_part)).perform(click())

        verify(navController).navigate(BodyPartsFragmentDirections.actionBodyPartsFragmentToBodyPartsAddFragment())
    }

}