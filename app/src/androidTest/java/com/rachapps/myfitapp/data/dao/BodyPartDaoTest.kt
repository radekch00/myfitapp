package com.rachapps.myfitapp.data.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.rachapps.myfitapp.data.MyFitDatabase
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.launchFragmentInHiltContainer
import com.rachapps.myfitapp.ui.bodyParts.add.BodyPartsAddFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named


@HiltAndroidTest
class BodyPartDaoTest {

    @get : Rule
    var hiltRule = HiltAndroidRule(this)

    @get : Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: MyFitDatabase

    private lateinit var bodyPartDao: BodyPartDao
    private lateinit var exerciseDao: ExerciseDao

    @Before
    fun setup() {
        hiltRule.inject()
        bodyPartDao = database.bodyPartDao()
        exerciseDao = database.exerciseDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertBodyPart() = runTest {
        val bodyPart = BodyPart("name", 1)
        bodyPartDao.insert(bodyPart)
        val bodyPartInsertedList = bodyPartDao.getAll("name").first()
        assertThat(bodyPartInsertedList).contains(bodyPart)
    }

    @Test
    fun deleteBodyPart() = runTest{
        val bodyPart = BodyPart("name", 1)
        bodyPartDao.insert(bodyPart)
        bodyPartDao.delete(bodyPart)
        val bodyPartInsertedList = bodyPartDao.getAll("name").first()
        assertThat(bodyPartInsertedList.size).isEqualTo(0)
    }

    @Test
    fun getBodyPartWithExercisesById() = runTest{
        val bodyPart = BodyPart("name", 1)
        bodyPartDao.insert(bodyPart)
        val exercise = Exercise("exercise", 1, 1)
        exerciseDao.insert(exercise)

        val bodyPartWithExercise = bodyPartDao.getBodyPartWithExercisesById(1).first()
        assertThat(bodyPartWithExercise.size).isEqualTo(1)

    }

    @Test
    fun getAllWithExercisesCountByBodyPartId() = runTest {
        val bodyPart = BodyPart("name", 1)
        bodyPartDao.insert(bodyPart)
        val exercise1 = Exercise("exercise", 1, 1)
        exerciseDao.insert(exercise1)
        val exercise2 = Exercise("exercise", 1, 2)
        exerciseDao.insert(exercise2)
        val exercise3 = Exercise("exercise", 1, 3)
        exerciseDao.insert(exercise3)

        var bodyPartWithExercisesCount = bodyPartDao.getAllWithExercisesCountByBodyPartId(1).first()
        assertThat(bodyPartWithExercisesCount[0].exercisesCount).isEqualTo(3)
    }

    @Test
    fun getAllWithExercisesCountByName() = runTest{
        val bodyPart = BodyPart("name", 1)
        bodyPartDao.insert(bodyPart)
        val exercise1 = Exercise("exercise", 1, 1)
        exerciseDao.insert(exercise1)
        val exercise2 = Exercise("exercise", 1, 2)
        exerciseDao.insert(exercise2)
        val bodyPart2 = BodyPart("other", 2)
        bodyPartDao.insert(bodyPart2)
        val exercise4 = Exercise("exercise", 2, 4)
        exerciseDao.insert(exercise4)
        val exercise5 = Exercise("exercise", 2, 5)
        exerciseDao.insert(exercise5)
        val exercise6 = Exercise("exercise", 2, 6)
        exerciseDao.insert(exercise6)

        var bodyParts = bodyPartDao.getAllWithExercisesCountByName("other").first()
        assertThat(bodyParts[0].exercisesCount).isEqualTo(3)
    }

    @Test
    fun getAll() = runTest{
        val bodyPart = BodyPart("name", 1)
        bodyPartDao.insert(bodyPart)
        val exercise1 = Exercise("exercise", 1, 1)
        exerciseDao.insert(exercise1)
        val exercise2 = Exercise("exercise", 1, 2)
        exerciseDao.insert(exercise2)
        val bodyPart2 = BodyPart("other", 2)
        bodyPartDao.insert(bodyPart2)
        val exercise4 = Exercise("exercise", 2, 4)
        exerciseDao.insert(exercise4)
        val exercise5 = Exercise("exercise", 2, 5)
        exerciseDao.insert(exercise5)
        val exercise6 = Exercise("exercise", 2, 6)
        exerciseDao.insert(exercise6)

        var bodyParts = bodyPartDao.getAll("the").first()
        assertThat(bodyParts[0]).isEqualTo(bodyPart2)
    }
}