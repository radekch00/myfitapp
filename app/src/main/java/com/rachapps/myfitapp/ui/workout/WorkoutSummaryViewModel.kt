package com.rachapps.myfitapp.ui.workout

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.dto.WorkoutSummaryDTO
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import com.rachapps.myfitapp.data.useCases.WorkoutSummaryDataUseCase
import com.rachapps.myfitapp.di.assistedFactories.WorkoutSummaryDataUseCaseFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WorkoutSummaryViewModel @Inject constructor(
    private val trainingRepository: TrainingRepository,
    private val state: SavedStateHandle,
    private var workoutSummaryDataUseCaseFactory: WorkoutSummaryDataUseCaseFactory
) : ViewModel(), WorkoutSummaryDataUseCase.OnSetSummaryListener {

    private val TAG = "WorkoutSummaryViewModel"

    val training = state.get<Training>("training")!!

    private val workoutSummaryChannel = Channel<WorkoutSummaryDTO> { }
    val workoutSummary = workoutSummaryChannel.receiveAsFlow()

    init {
        val wsu = workoutSummaryDataUseCaseFactory.create(training, viewModelScope, this)
        wsu.execute()
    }

    fun test() {
        Log.d(TAG, "test: tets")
    }

    override fun onSetSummaryData(workoutSummaryDTO: WorkoutSummaryDTO) {
        Log.d(TAG, "onSetSummaryData: $workoutSummaryDTO")
        viewModelScope.launch {
            workoutSummaryChannel.send(workoutSummaryDTO)
        }
    }
}