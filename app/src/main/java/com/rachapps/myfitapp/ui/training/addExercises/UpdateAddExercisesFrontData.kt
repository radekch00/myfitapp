package com.rachapps.myfitapp.ui.training.addExercises

import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo

interface UpdateAddExercisesFrontData {
    fun onExercisesWithSetDTOUpdated(list: MutableList<ExerciseWithSetNo>)
}