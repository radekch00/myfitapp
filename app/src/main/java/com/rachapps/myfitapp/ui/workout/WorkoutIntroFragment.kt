package com.rachapps.myfitapp.ui.workout

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.databinding.FragmentIntroBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WorkoutIntroFragment : Fragment(), SetsAdapter.OnItemClickListener {

    private val TAG = "WorkoutIntroFragment"

    private val viewModelWorkout: WorkoutIntroViewModel by viewModels()

    lateinit var binding: FragmentIntroBinding

    @Inject
    lateinit var setsAdapter: SetsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_workout_intro, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentIntroBinding.bind(view)

        binding.fabStartWorkout.apply {
            setOnClickListener {
                viewModelWorkout.onNextButtonClicked()
            }
        }

        collectLatestLifecycleFlow(viewModelWorkout.trainingState) { training ->
            binding.apply {
                textViewTrainingName.text = training.title
                textViewDescription.text = training.description
                textViewDate.text = training.date.toString()
            }
        }

        binding.apply {
            recyclerViewSets.apply {
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                adapter = setsAdapter
            }
        }

        collectLatestLifecycleFlow(viewModelWorkout.sets) {
            setsAdapter.submitList(it)
        }

        collectLatestLifecycleFlow(viewModelWorkout.exerciseIntroEvent) { event ->
            when (event) {
                is WorkoutIntroViewModel.WorkoutIntroEvents.NavigateToExercise -> {
                    findNavController().navigate(
                        WorkoutIntroFragmentDirections.actionIntroToExercise(
                            training = event.training,
                            set = event.setEntity
                        )
                    )
                }
            }.exhaustive
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_workout_up -> {
                findNavController().navigate(WorkoutIntroFragmentDirections.actionGlobalTrainingsFragment())
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }

    }

    override fun onItemClick(setAndExercise: SetAndExercise) {
        Log.d(TAG, "onItemClick: $setAndExercise")
        viewModelWorkout.onSetClicked(setAndExercise)
    }
}