package com.rachapps.myfitapp.ui.bodyParts.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.dao.BodyPartDao
import com.rachapps.myfitapp.data.entities.relations.BodyPartsWithExercisesCount
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BodyPartsViewModel @Inject constructor(
    private val bodyPartDao: BodyPartDao
) : ViewModel() {

    private val searchQuery = MutableStateFlow("")
    val sortOrder = MutableStateFlow(SortOrder.BY_NAME)

    fun onChangeSearchQuery(query: String) {
        searchQuery.value = query
    }

    @ExperimentalCoroutinesApi
    val bodyPartsFlow = searchQuery.flatMapLatest {
        bodyPartDao.getAllWithExercisesCountByName(it)
    }

    private val bodyPartsEventChannel = Channel<BodyPartsEvents> { }
    val bodyPartsEvent = bodyPartsEventChannel.receiveAsFlow()

    fun onItemSelected(bodyPartCount: BodyPartsWithExercisesCount) {
        viewModelScope.launch() {
            bodyPartsEventChannel.send(
                BodyPartsEvents.NavigateToEditBodyPart(
                    bodyPartCount
                )
            )
        }
    }

    fun onBodyPartSwapped(bodyPartsWithExercisesCount : BodyPartsWithExercisesCount){
        viewModelScope.launch {
            if(bodyPartsWithExercisesCount.exercisesCount > 0){
                bodyPartsEventChannel.send(BodyPartsEvents.ShowInvalidOperationMessage("Cannot delete - please remove exercises first"))
            }else{
                bodyPartDao.delete(bodyPartsWithExercisesCount.bodyPart)
                bodyPartsEventChannel.send(BodyPartsEvents.ShowUndoDeleteExerciseMessage(bodyPartsWithExercisesCount))
            }
        }
    }

    fun onUndoDeleteClick(bodyPartsWithExercisesCount : BodyPartsWithExercisesCount){
        viewModelScope.launch {
            bodyPartsWithExercisesCount.bodyPart?.let { bodyPartDao.insert(it) }
        }
    }

    enum class SortOrder { BY_NAME }

    sealed class BodyPartsEvents {
        data class NavigateToEditBodyPart(val bodyPartCount: BodyPartsWithExercisesCount) : BodyPartsEvents()
        data class ShowInvalidOperationMessage(val msg: String) : BodyPartsEvents()
        data class ShowUndoDeleteExerciseMessage(val bodyPartCount: BodyPartsWithExercisesCount) : BodyPartsEvents()
    }
}