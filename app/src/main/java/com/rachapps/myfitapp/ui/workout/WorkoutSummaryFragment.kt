package com.rachapps.myfitapp.ui.workout

import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.databinding.FragmentWorkoutSummaryBinding
import com.rachapps.myfitapp.databinding.ItemRecordBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WorkoutSummary : Fragment() {

    private val viewModel: WorkoutSummaryViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.test()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout_summary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = FragmentWorkoutSummaryBinding.bind(view)

        collectLatestLifecycleFlow(viewModel.workoutSummary) { workoutSummaryDTO ->
            binding.apply {
                textViewTrainingName.text = workoutSummaryDTO.training.title
                textViewDescription.text = workoutSummaryDTO.training.description
                textViewDate.text = workoutSummaryDTO.training.date.toString()
                textViewWarmUpNo.text = workoutSummaryDTO.warmUpNo.toString()
                textViewWarmUpWeight.text = workoutSummaryDTO.warmUpWeight.toString()
                textViewWorkingNo.text = workoutSummaryDTO.workingSetsNo.toString()
                textViewWorkingWeight.text = workoutSummaryDTO.workingSetsWeight.toString()
                textViewTotalNo.text = workoutSummaryDTO.setNo.toString()
                textViewTotalWeight.text = workoutSummaryDTO.totalWeight.toString()
                linearLayoutRecordsContainer.apply {
                    workoutSummaryDTO.records.forEach {
                        var binding = ItemRecordBinding.inflate(
                            LayoutInflater.from(context),
                            linearLayoutRecordsContainer,
                            false
                        )
                        binding.apply {
                            textViewExerciseName.text = it.currentWeight.exercise.name
                            textViewCurrentWorkoutWeight.text =
                                formatRecordRow(it.currentWeight.set)
                            textViewRecordWorkoutWeight.text = formatRecordRow(it.maxWeight.set)
                        }
                        addView(binding.root)
                    }
                }
            }
        }
    }

    private fun formatRecordRow(set: SetEntity): String {
        return getString(
            R.string.record_row,
            set.repeats.toString(),
            set.weight.toString()
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_workout_summary, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_workout_up -> {
                findNavController().navigate(WorkoutSummaryDirections.actionGlobalTrainingsFragment())
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}