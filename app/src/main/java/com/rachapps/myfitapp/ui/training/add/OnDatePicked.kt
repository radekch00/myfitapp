package com.rachapps.myfitapp.ui.training.add

interface OnDatePicked {
    fun onDatePicked(year : Int, month : Int, day : Int)
}