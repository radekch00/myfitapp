package com.rachapps.myfitapp.ui.training.sortSets

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import com.rachapps.myfitapp.data.useCases.SortSetsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.time.LocalTime
import javax.inject.Inject

@HiltViewModel
class SortSetsViewModel @Inject constructor(
    private val trainingRepository: TrainingRepository,
    private val state : SavedStateHandle
) : ViewModel(), SortSetsUseCase.OnOrderChanged {

    private val TAG = "SortSetsViewModel"

    private val training = state.get<Training>("training")!!

    val sets = trainingRepository.getSetAndExercise(trainingId = training.id)

    private val sortSetsEventsChannel = Channel<SortSetsEvents>()
    val sortSetsEvent = sortSetsEventsChannel.receiveAsFlow()

    fun onSetSwiped(set : SetAndExercise){
        viewModelScope.launch {
            trainingRepository.deleteSet(set.set)
            sortSetsEventsChannel.send(SortSetsEvents.ShowUndoDeleteSetMessage(set))
        }
    }

    fun onUndoDeleteClick(set: SetAndExercise){
        viewModelScope.launch {
            trainingRepository.insertSet(set.set)
            sortSetsEventsChannel.send(SortSetsEvents.OnUndoDelete("Set is back!"))
        }
    }

    fun onSetMoved(setDragged : Int, setDropped : Int){
        var time = LocalTime.now()
        Log.d(TAG, "onSetMoved: $setDragged to $setDropped $time")
        //TODO: To działa tylko dlatego, ze jest  1 header. Przerobić, żeby było uniwersalne
        SortSetsUseCase(trainingRepository, viewModelScope, this).execute(training.id, setDragged -1, setDropped-1)
    }

    fun onWarmUpCheckBoxClicked(set: SetAndExercise, checked : Boolean){
        viewModelScope.launch {
            var updatedSet = set.set.copy(warmUp = checked)
            trainingRepository.saveSet(updatedSet)
        }
    }

    sealed class SortSetsEvents {
        data class NavigateToTrainingsList(val msg : String) : SortSetsEvents()
        data class ShowUndoDeleteSetMessage(val SetAndExercise: SetAndExercise) : SortSetsEvents()
        data class OnUndoDelete(val msg : String) : SortSetsEvents()
        data class OnOrderChanged(val refresh : Boolean) : SortSetsEvents()
    }

    override fun onOrderChanged() {
        viewModelScope.launch {
            sortSetsEventsChannel.send(SortSetsEvents.OnOrderChanged(true))
        }
    }
}