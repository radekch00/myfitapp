package com.rachapps.myfitapp.ui.training.add

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import com.rachapps.myfitapp.data.useCases.CreateOrUpdateTrainingEntityUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import javax.inject.Inject

@HiltViewModel
class TrainingAddViewModel @Inject constructor(
    private val trainingRepository: TrainingRepository,
    private val state: SavedStateHandle
) : ViewModel() {

    private val savedTraining = state.get<Training>("training")

    var name = state.get<String>("name") ?: savedTraining?.title ?: ""
        set(value) {
            field = value
            state["name"] = value
        }

    var description = state.get<String>("description") ?: savedTraining?.description ?: ""
        set(value) {
            field = value
            state["description"] = value
        }

    var date =
        state.get<String>("date") ?: savedTraining?.date?.toString() ?: DefaultTrainingDate.get()
        set(value) {
            field = value
            state["date"] = value
        }


    private val addTrainingChannel = Channel<TrainingAddEvent>()
    val addTrainingEvent = addTrainingChannel.receiveAsFlow()

    fun onSaveTrainingClicked(name: String, description: String, date: String) {
        viewModelScope.launch {

            var training = CreateOrUpdateTrainingEntityUseCase(
                savedTraining,
                Training(date = LocalDate.parse(date), title = name, description = description)
            ).execute()

            if (training.isValid()) {
                training = trainingRepository.saveTraining(training)
                navigateToAddExercises(training)
            } else {
                showInvalidInputMessage("Title name cannot be empty")
            }
        }
    }

    private suspend fun navigateToAddExercises(training: Training) {
        addTrainingChannel.send(
            TrainingAddEvent.NavigateToAddExercisesWithResults(
                training
            )
        )
    }

    private suspend fun showInvalidInputMessage(message: String) {
        addTrainingChannel.send(TrainingAddEvent.ShowInValidInputMessage(message))
    }

    sealed class TrainingAddEvent {
        class NavigateToAddExercisesWithResults(val training: Training) : TrainingAddEvent()
        class ShowInValidInputMessage(val message: String) : TrainingAddEvent()
    }

    companion object DefaultTrainingDate {
        fun get(): String {
            return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
        }
    }

}