package com.rachapps.myfitapp.ui.bodyParts.add

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.dao.BodyPartDao
import com.rachapps.myfitapp.data.entities.relations.BodyPartsWithExercisesCount
import com.rachapps.myfitapp.ui.ADD_BODY_PART_RESULT_OK
import com.rachapps.myfitapp.ui.EDIT_BODY_PART_RESULT_OK
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BodyPartsAddViewModel @Inject constructor(
    val bodyPartDao: BodyPartDao,
    private val state: SavedStateHandle
) : ViewModel() {

    val bodyPart = state.get<BodyPartsWithExercisesCount>("bodyPart")

    private val _addBodyPartMode = MutableStateFlow(bodyPart == null)

    val addBodyPartMode : StateFlow<Boolean> = _addBodyPartMode

    var name = state.get<String>("name") ?: bodyPart?.bodyPart?.name ?: ""
        set(value) {
            field = value
            state["name"] = value
        }

    fun onSaveClicked(name: String) {
        viewModelScope.launch {
            if (name.isEmpty()) {
                addEditBodyPartChannel.send(BodyPartsAddEvent.ShowInvalidInputMessage("Name cannot be empty"))
            } else {
                bodyPart?.let {
                    bodyPartDao.update(BodyPart(name, it.bodyPart.id))
                    addEditBodyPartChannel.send(
                        BodyPartsAddEvent.NavigateBackWithResult(
                            EDIT_BODY_PART_RESULT_OK
                        )
                    )
                } ?: run {
                    bodyPartDao.insert(BodyPart(name))
                    addEditBodyPartChannel.send(
                        BodyPartsAddEvent.NavigateBackWithResult(
                            ADD_BODY_PART_RESULT_OK
                        )
                    )
                }
            }
        }
    }

    private val addEditBodyPartChannel = Channel<BodyPartsAddEvent>()
    val addEditTaskEvent = addEditBodyPartChannel.receiveAsFlow()

    sealed class BodyPartsAddEvent {
        data class ShowInvalidInputMessage(val msg: String) : BodyPartsAddEvent()
        data class NavigateBackWithResult(val result: Int) : BodyPartsAddEvent()
    }
}