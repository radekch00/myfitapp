package com.rachapps.myfitapp.ui.training.addExercises

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import com.rachapps.myfitapp.databinding.FragmentTrainingAddExercisesBinding
import com.rachapps.myfitapp.di.ApplicationScope
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@AndroidEntryPoint
class TrainingAddExercisesFragment : Fragment(R.layout.fragment_training_add_exercises),
    ExercisesInTrainingAdapter.OnChangeNoOfSetClickListener {

    private val addTrainingExercisesViewModel: TrainingAddExercisesViewModel by viewModels()

    @Inject
    lateinit var exercisesAdapter: ExercisesInTrainingAdapter

    @Inject
    @ApplicationScope
    lateinit var applicationScope: CoroutineScope

    private val TAG = "TrainingAddFragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentTrainingAddExercisesBinding.bind(view)

        binding.apply {
            fabSaveTraining.setOnClickListener {
                addTrainingExercisesViewModel.onSaveTrainingClicked()
            }

            recyclerViewExercises.apply {
                adapter = exercisesAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }


        collectLatestLifecycleFlow(addTrainingExercisesViewModel.exercises) {
            Log.d(TAG, "onViewCreated: Collect1")
            exercisesAdapter.submitList(it)
            //exercisesAdapter.notifyDataSetChanged()
        }

        collectLatestLifecycleFlow(addTrainingExercisesViewModel.addEditTrainingEvent) { event ->
            @Suppress("IMPLICIT_CAST_TO_ANY")
            when (event)  {
                is TrainingAddExercisesViewModel.TrainingAddExercisesEvent.NavigateBackWithResult -> {
                    setFragmentResult(
                        "add_edit_training",
                        bundleOf("add_edit_training" to event.result)
                    )
                    findNavController().popBackStack()
                }
                is TrainingAddExercisesViewModel.TrainingAddExercisesEvent.NavigateToSortSetsList -> {
                    setFragmentResult(
                        "training", bundleOf("training" to event.training)
                    )
                    findNavController().navigate(TrainingAddExercisesFragmentDirections.actionTrainingAddExercisesFragmentToSortSetsFragment(event.training))
                }
            }.exhaustive
        }
    }

    override fun onAddSet(exercise: ExerciseWithSetNo) {
        addTrainingExercisesViewModel.onAddSetClicked(exercise)
    }

    override fun onRemoveSet(exercise: ExerciseWithSetNo) {
        addTrainingExercisesViewModel.onRemoveSetClicked(exercise)
    }
}