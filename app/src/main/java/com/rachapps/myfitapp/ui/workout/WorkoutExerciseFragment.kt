package com.rachapps.myfitapp.ui.workout

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.databinding.FragmentExerciseBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WorkoutExerciseFragment : Fragment() {

    private val viewModel: WorkoutExerciseViewModel by viewModels()


    private val TAG = "WorkoutExerciseFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_workout_exercise, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = FragmentExerciseBinding.bind(view)

        binding.apply {
            fabStartWorkout.setOnClickListener {
                val weight = editTextWeight.text.toString()
                val repeats = editTextRepeats.text.toString()
                viewModel.onNextClicked(
                    weight = if (weight.isNotEmpty()) weight.toDouble() else 0.0,
                    repeats = if (repeats.isNotEmpty()) repeats.toInt() else 0,
                    warmUp = checkBoxWarmUp.isChecked
                )
            }
        }

        //Set up saved values
        collectLatestLifecycleFlow(viewModel.currentSetData) { setAndExercise ->
            binding.apply {
                textViewExerciseName.text = setAndExercise.exercise.name
                if (!setAndExercise.set.weight.equals(0.0)) {
                    editTextWeight.setText(setAndExercise.set.weight.toString())
                    editTextRepeats.setText(setAndExercise.set.repeats.toString())
                }
                checkBoxWarmUp.isChecked = setAndExercise.set.warmUp
            }
        }

        collectLatestLifecycleFlow(viewModel.workoutExerciseEvent) { event ->
            when (event) {
                is WorkoutExerciseViewModel.WorkoutExercisesEvents.NavigateToSummary -> {
                    findNavController().navigate(
                        WorkoutExerciseFragmentDirections.actionExerciseToWorkoutSummary(
                            training = event.training
                        )
                    )
                }
                is WorkoutExerciseViewModel.WorkoutExercisesEvents.NavigateToExercise -> {
                    findNavController().navigate(
                        WorkoutExerciseFragmentDirections.actionExerciseSelf(
                            training = event.training,
                            set = event.setEntity
                        )
                    )
                }
                is WorkoutExerciseViewModel.WorkoutExercisesEvents.NavigateToIntro -> {
                    findNavController().navigate(
                        WorkoutExerciseFragmentDirections.actionExerciseToWorkout(event.training)
                    )
                }
            }.exhaustive
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d(TAG, "onOptionsItemSelected: clicked")
        return when (item.itemId) {
            R.id.menu_item_workout_start -> {
                viewModel.onUpClicked()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}