package com.rachapps.myfitapp.ui.training.addExercises

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import com.rachapps.myfitapp.databinding.ItemExerciseAddToTrainingBinding
import javax.inject.Inject


class ExercisesInTrainingAdapter @Inject constructor(private val listener : OnChangeNoOfSetClickListener) :
    ListAdapter<ExerciseWithSetNo, ExercisesInTrainingAdapter.ExercisesViewHolder>(
        Callback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExercisesViewHolder {
        val binding = ItemExerciseAddToTrainingBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ExercisesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ExercisesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ExercisesViewHolder(private val binding: ItemExerciseAddToTrainingBinding) :
        RecyclerView.ViewHolder(binding.root) {


        init{
            binding.apply {
                buttonAddSet.setOnClickListener{
                    listener.onAddSet(getItem(adapterPosition))
                }

                buttonRemoveSet.setOnClickListener{
                    listener.onRemoveSet(getItem(adapterPosition))
                }
            }
        }

        fun bind(exercise: ExerciseWithSetNo) {
            binding.apply {
                textViewName.text = exercise.exercise!!.name
                textViewSetsCounter.text = exercise.setNo.toString()
            }
        }
    }

    class Callback : DiffUtil.ItemCallback<ExerciseWithSetNo>() {
        override fun areItemsTheSame(oldItem: ExerciseWithSetNo, newItem: ExerciseWithSetNo): Boolean {
            return oldItem.exercise!!.id == newItem.exercise!!.id
        }

        override fun areContentsTheSame(oldItem: ExerciseWithSetNo, newItem: ExerciseWithSetNo): Boolean {
            return oldItem == newItem
        }
    }

    interface OnChangeNoOfSetClickListener {
        fun onAddSet(exercise: ExerciseWithSetNo)
        fun onRemoveSet(exercise: ExerciseWithSetNo)
    }
}