package com.rachapps.myfitapp.ui.bodyParts.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rachapps.myfitapp.data.entities.relations.BodyPartsWithExercisesCount
import com.rachapps.myfitapp.databinding.ItemBodyPartsBinding
import javax.inject.Inject

class BodyPartsAdapter @Inject constructor(private val listener: OnItemClickListener) :
    ListAdapter<BodyPartsWithExercisesCount, BodyPartsAdapter.BodyPartsViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BodyPartsViewHolder {
        val binding =
            ItemBodyPartsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BodyPartsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BodyPartsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class BodyPartsViewHolder(private val binding: ItemBodyPartsBinding) :
        RecyclerView.ViewHolder(binding.root) {


        init{
            binding.apply {
                root.setOnClickListener{
                    val position = adapterPosition
                    if(position != RecyclerView.NO_POSITION){
                        listener.onItemClick(getItem(position))
                    }
                }
            }
        }

        fun bind(bodyPartCount: BodyPartsWithExercisesCount) {
            binding.textViewName.text = bodyPartCount.bodyPart.name
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<BodyPartsWithExercisesCount>() {
        override fun areItemsTheSame(oldItem: BodyPartsWithExercisesCount, newItem: BodyPartsWithExercisesCount): Boolean {
            return oldItem.bodyPart.id == newItem.bodyPart.id
        }

        override fun areContentsTheSame(oldItem: BodyPartsWithExercisesCount, newItem: BodyPartsWithExercisesCount): Boolean {
            return oldItem == newItem
        }
    }

    interface OnItemClickListener{
        fun onItemClick(bodyPartCount: BodyPartsWithExercisesCount)
    }

}