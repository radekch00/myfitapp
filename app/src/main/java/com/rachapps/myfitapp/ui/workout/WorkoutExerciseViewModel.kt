package com.rachapps.myfitapp.ui.workout

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.repositories.ITrainingRepository
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WorkoutExerciseViewModel @Inject constructor(
    private val state: SavedStateHandle,
    private val repository: ITrainingRepository
) : ViewModel() {

    private val TAG = "WorkoutExerciseViewMode"
    private val training = state.get<Training>("training")

    private var currentSet = state.get<SetEntity>("set")
    private var currentExercise: Exercise? = null
    private var nextSet: SetEntity? = null
    private var nextExercise: Exercise? = null

    private var weight = state.get<Double>("weight") ?: currentSet?.weight
        set(value) {
            field = value
            state["weight"] = value
        }

    private var repeats = state.get<Int>("repeats") ?: currentSet?.repeats
        set(value) {
            field = value
            state["repeats"] = value
        }

    private var warmUp = state.get<Boolean>("warmUp") ?: currentSet?.warmUp ?: false
        set(value) {
            field = value
            state["warmUp"] = value
        }


    private val workoutExerciseChannel = Channel<WorkoutExercisesEvents>()
    val workoutExerciseEvent = workoutExerciseChannel.receiveAsFlow()

    /*
    * We need also next set to know where to navigate on next clicked
    */
    private val currentAndNextSet = repository.getSetAndExercise(
        trainingId = training!!.id,
        order = currentSet?.order,
        limit = 2
    )

    /*
    We could collect currentAndNextSet in Fragment but it is nicer to send only data required by view
     */
    private val currentSetChannel = Channel<SetAndExercise>()
    val currentSetData = currentSetChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            currentAndNextSet.collectLatest { setAndExerciseList ->
                setUpCurrentAndNextSetAndExercise(setAndExerciseList)
                emitCurrentSetAndExercise(setAndExerciseList)
                cancel()
            }
        }
    }

    private fun setUpCurrentAndNextSetAndExercise(setAndExercise: List<SetAndExercise>) {
        //Log.d(TAG, "setUpCurrentAndNextSetAndExercise: $setAndExercise")
        if (setAndExercise.isNotEmpty()) {
            currentSet = setAndExercise[0].set
            currentExercise = setAndExercise[0].exercise
        }
        if (setAndExercise.size > 1) {
            nextSet = setAndExercise[1].set
            nextExercise = setAndExercise[1].exercise
        }
    }

    private suspend fun emitCurrentSetAndExercise(setAndExercise: List<SetAndExercise>) {
        if (setAndExercise.isNotEmpty()) {
            currentSetChannel.send(setAndExercise[0])
        }
    }

    fun onNextClicked(weight: Double, repeats: Int, warmUp: Boolean) {
        viewModelScope.launch {
            val setDone = currentSet?.copy(weight = weight, repeats = repeats, warmUp = warmUp)
            repository.saveSet(setDone!!)
            if (nextSet != null) workoutExerciseChannel.send(
                WorkoutExercisesEvents.NavigateToExercise(
                    training!!,
                    nextSet!!
                )
            )
            else {
                workoutExerciseChannel.send(WorkoutExercisesEvents.NavigateToSummary(training!!))
            }
        }
    }

    fun onUpClicked() {
        Log.d(TAG, "onUpClicked: clocked")
        viewModelScope.launch {
            workoutExerciseChannel.send(WorkoutExercisesEvents.NavigateToIntro(training!!))
        }
    }

    sealed class WorkoutExercisesEvents {
        class NavigateToExercise(val training: Training, val setEntity: SetEntity) :
            WorkoutExercisesEvents()

        class NavigateToSummary(val training: Training) : WorkoutExercisesEvents()
        class NavigateToIntro(val training: Training) : WorkoutExercisesEvents()
    }

}