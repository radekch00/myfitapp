package com.rachapps.myfitapp.ui.bodyParts.list

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.relations.BodyPartsWithExercisesCount
import com.rachapps.myfitapp.databinding.FragmentBodyPartsBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BodyPartsFragment : Fragment(R.layout.fragment_body_parts),
    BodyPartsAdapter.OnItemClickListener {

    private val viewModel: BodyPartsViewModel by viewModels()

    @Inject
    lateinit var bodyPartsAdapter: BodyPartsAdapter

    private val TAG = "BodyPartsFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentBodyPartsBinding.bind(view)

        requireActivity().title = resources.getString(R.string.muscle_groups)

        binding.apply {
            fabAddBodyPart.apply {
                setOnClickListener {
                    findNavController().navigate(BodyPartsFragmentDirections.actionBodyPartsFragmentToBodyPartsAddFragment())
                }
            }

            recyclerViewBodyParts.apply {
                adapter = bodyPartsAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        collectLatestLifecycleFlow(viewModel.bodyPartsFlow) {
            bodyPartsAdapter.submitList(it)
        }

        collectLatestLifecycleFlow(viewModel.bodyPartsEvent) { event ->
            when (event) {
                is BodyPartsViewModel.BodyPartsEvents.NavigateToEditBodyPart -> {
                    findNavController().navigate(
                        BodyPartsFragmentDirections.actionBodyPartsFragmentToBodyPartsAddFragment(
                            event.bodyPartCount
                        )
                    )
                }
                is BodyPartsViewModel.BodyPartsEvents.ShowInvalidOperationMessage -> {
                    bodyPartsAdapter.notifyDataSetChanged()
                    Snackbar.make(requireView(), event.msg, Snackbar.LENGTH_LONG).show()
                }
                is BodyPartsViewModel.BodyPartsEvents.ShowUndoDeleteExerciseMessage -> {
                    Snackbar.make(requireView(), "Body part has been deleted", Snackbar.LENGTH_LONG)
                        .setAction("UNDO") { viewModel.onUndoDeleteClick(event.bodyPartCount) }.show()
                }
            }.exhaustive
        }


        setFragmentResultListener("add_edit_request") { _, bundle ->
            val result = bundle.getInt("add_edit_result")
            //TODO: Call viewmodel to do smth on added or updated results
            Log.d(TAG, "onAddOrEditBodyPart")

        }



        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }


            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val bodyPart = bodyPartsAdapter.currentList[viewHolder.adapterPosition]
                viewModel.onBodyPartSwapped(bodyPart)
            }
        }).attachToRecyclerView(binding.recyclerViewBodyParts)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_body_parts, menu)

        val searchView = menu.findItem(R.id.menu_item_search_body_part).actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.onChangeSearchQuery(newText.orEmpty())
                return true
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_exercises -> {
                findNavController().navigate(BodyPartsFragmentDirections.actionBodyPartsFragmentToExercisesFragment())
                true
            }
            R.id.menu_item_trainings ->{
                findNavController().navigate(BodyPartsFragmentDirections.actionBodyPartsFragmentToTrainingsFragment())
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onItemClick(bodyPartCount: BodyPartsWithExercisesCount) {
        viewModel.onItemSelected(bodyPartCount)
    }
}