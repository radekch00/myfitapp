package com.rachapps.myfitapp.ui.training.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.dao.TrainingDao
import com.rachapps.myfitapp.data.entities.Training
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TrainingsViewModel @Inject constructor(private val trainingDao: TrainingDao) :ViewModel() {


    val trainings = trainingDao.selectAll()

    private val trainingsEventChannel = Channel<TrainingsEvent>()
    val trainingsEvent = trainingsEventChannel.receiveAsFlow()


    fun onTrainingSelected(training : Training){
        viewModelScope.launch {
            trainingsEventChannel.send(TrainingsEvent.OnTrainingSelected(training))
        }
    }

    fun onEditTrainingSelected(training : Training){
        viewModelScope.launch {
            trainingsEventChannel.send(TrainingsEvent.OnEditTrainingSelected(training))
        }
    }

    sealed class TrainingsEvent{
        class OnTrainingSelected(val training : Training) : TrainingsEvent()
        class OnEditTrainingSelected(val training : Training) : TrainingsEvent()
    }

}