package com.rachapps.myfitapp.ui.training.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.databinding.ItemTrainingsBinding
import com.rachapps.myfitapp.ui.exercises.list.ExerciseAdapter
import javax.inject.Inject

class TrainingsAdapter @Inject constructor(private val listener : TrainingsAdapter.OnItemClickListener) : ListAdapter<Training, TrainingsAdapter.TrainingsViewHolder>(
    Callback()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainingsViewHolder {
        val binding = ItemTrainingsBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return TrainingsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TrainingsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class TrainingsViewHolder(private val binding: ItemTrainingsBinding) : RecyclerView.ViewHolder(binding.root){

        init{
            binding.apply {
                root.clipToOutline = true
                root.setOnClickListener{
                    val training = getItem(adapterPosition)
                    listener.onItemClick(training, ItemClickType.ROOT)
                }
                buttonEditTraining.setOnClickListener{
                    val training = getItem(adapterPosition)
                    listener.onItemClick(training, ItemClickType.EDIT_BUTTON)
                }
            }
        }

        fun bind(training : Training){
            binding.textViewName.text = training.title
            binding.textViewDescription.text = training.description
        }

    }

    class Callback : DiffUtil.ItemCallback<Training>(){
        override fun areItemsTheSame(oldItem: Training, newItem: Training): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Training, newItem: Training): Boolean {
            return oldItem == newItem
        }
    }

    interface OnItemClickListener{
        fun onItemClick(training : Training, click : ItemClickType)
    }

    enum class ItemClickType{
        EDIT_BUTTON,
        ROOT
    }
}