package com.rachapps.myfitapp.ui.workout

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.remote.MyFitAPI
import com.rachapps.myfitapp.data.repositories.ITrainingRepository
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WorkoutIntroViewModel @Inject constructor(
    private val trainingRepository: ITrainingRepository,
    private val state: SavedStateHandle,
    private val api : MyFitAPI
) :
    ViewModel() {

    private val TAG = "WorkoutIntroViewModel"

    private val training = state.get<Training>("training")

    private val _trainingState = MutableStateFlow<Training>(training!!)
    val trainingState :StateFlow<Training> = _trainingState

    private val exerciseIntroChannel = Channel<WorkoutIntroEvents> {  }
    val exerciseIntroEvent = exerciseIntroChannel.receiveAsFlow()


    val sets = trainingRepository.getSetAndExercise(trainingId = training!!.id)


    fun onNextButtonClicked(){
        viewModelScope.launch {
            exerciseIntroChannel.send(WorkoutIntroEvents.NavigateToExercise(training!!))
        }
    }

    fun onSetClicked(setAndExercise: SetAndExercise){
        viewModelScope.launch {
            exerciseIntroChannel.send(WorkoutIntroEvents.NavigateToExercise(training!!, setAndExercise.set))
        }
    }

    sealed class WorkoutIntroEvents{
        class NavigateToExercise(val training: Training, val setEntity: SetEntity? = null) : WorkoutIntroEvents()
    }


}