package com.rachapps.myfitapp.ui.exercises.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.databinding.ItemExerciseBinding
import javax.inject.Inject

class ExerciseAdapter @Inject constructor (private val listener : OnItemClickListener) : ListAdapter<Exercise, ExerciseAdapter.ExerciseViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseViewHolder {
        val binding = ItemExerciseBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ExerciseViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ExerciseViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class ExerciseViewHolder(private val binding : ItemExerciseBinding) : RecyclerView.ViewHolder(binding.root){

        init{
            binding.apply {
                root.setOnClickListener{
                    val position = adapterPosition
                    if(position != RecyclerView.NO_POSITION){
                        val exercise = getItem(position)
                        listener.onItemClick(exercise)
                    }
                }
            }
        }

        fun bind(item : Exercise){
            binding.textViewName.text = item.name
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Exercise>(){
        override fun areItemsTheSame(oldItem: Exercise, newItem: Exercise): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Exercise, newItem: Exercise): Boolean {
            return oldItem == newItem
        }
    }

    interface OnItemClickListener{
        fun onItemClick(exercise: Exercise)
    }
}