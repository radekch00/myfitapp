package com.rachapps.myfitapp.ui.workout


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.databinding.ItemSetsBinding
import javax.inject.Inject

class SetsAdapter @Inject constructor(private val listener : OnItemClickListener) :
    ListAdapter<SetAndExercise, SetsAdapter.ViewHolder>(Callback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSetsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: ItemSetsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init{
            binding.root.setOnClickListener {
                val position = adapterPosition
                if(position != RecyclerView.NO_POSITION){
                    listener.onItemClick(getItem(position))
                }
            }
        }

        fun bind(set: SetAndExercise) {
            binding.apply {
                textViewName.text = set.exercise.name
                if (set.set.repeats != 0 && set.set.weight != 0.0) {
                    textViewReps.text = set.set.repeats.toString()
                    textViewWeight.text = set.set.weight.toString()
                } else {
                    textViewX.visibility = View.GONE
                }
            }
        }
    }


    class Callback : DiffUtil.ItemCallback<SetAndExercise>() {
        override fun areItemsTheSame(oldItem: SetAndExercise, newItem: SetAndExercise): Boolean {
            return oldItem.set.id == newItem.set.id
        }

        override fun areContentsTheSame(oldItem: SetAndExercise, newItem: SetAndExercise): Boolean {
            return oldItem == newItem
        }
    }

    interface OnItemClickListener{
        fun onItemClick(setAndExercise : SetAndExercise)
    }
}