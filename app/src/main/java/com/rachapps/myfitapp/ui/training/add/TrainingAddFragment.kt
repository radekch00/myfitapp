package com.rachapps.myfitapp.ui.training.add

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.databinding.FragmentTrainingAddBinding
import com.rachapps.myfitapp.ui.training.list.TrainingsViewModel
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

@AndroidEntryPoint
class TrainingAddFragment : Fragment(), OnDatePicked {


    private val addViewModel: TrainingAddViewModel by viewModels()
    lateinit var binding: FragmentTrainingAddBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_training_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentTrainingAddBinding.bind(view)

        binding.apply {
            fabSaveTraining.setOnClickListener {
//                Training(
//                    LocalDate.parse(
//                        editTextDate.text.toString(),
//                        DateTimeFormatter.ISO_DATE
//                    ), editTextTrainingName.text.toString(), editTextDescription.text.toString()
//                )
                addViewModel.onSaveTrainingClicked(
                    editTextTrainingName.text.toString(),
                    editTextDescription.text.toString(),
                    editTextDate.text.toString()
                )
            }

            editTextDate.setText(addViewModel.date)
            editTextDate.setOnClickListener {
                val datePicker = DatePicker(this@TrainingAddFragment)
                datePicker.show(parentFragmentManager, "DATE_PICK")
            }
            editTextTrainingName.setText(addViewModel.name)
            editTextDescription.setText(addViewModel.description)

        }

        collectLatestLifecycleFlow(addViewModel.addTrainingEvent) { event ->
            when (event) {
                is TrainingAddViewModel.TrainingAddEvent.NavigateToAddExercisesWithResults -> {
                    findNavController().navigate(
                        TrainingAddFragmentDirections.actionAddTrainingFragmentToTrainingAddExercisesFragment(
                            event.training
                        )
                    )
                }
                is TrainingAddViewModel.TrainingAddEvent.ShowInValidInputMessage -> {
                    Snackbar.make(requireView(), event.message, Snackbar.LENGTH_LONG).show()
                }
            }.exhaustive
        }
    }

    override fun onDatePicked(year: Int, month: Int, day: Int) {
        binding.apply {
            editTextDate.setText(
                getString(
                    R.string.date_format,
                    year.toString(),
                    month.toString().padStart(2, '0'),
                    day.toString().padStart(2, '0')
                )
            )
        }
    }
}