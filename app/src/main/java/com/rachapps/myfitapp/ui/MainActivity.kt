package com.rachapps.myfitapp.ui

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.ui.splash.SplashScreenViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

const val ADD_BODY_PART_RESULT_OK = Activity.RESULT_FIRST_USER
const val EDIT_BODY_PART_RESULT_OK = Activity.RESULT_FIRST_USER + 1
const val ADD_EXERCISE_RESULT_OK = Activity.RESULT_FIRST_USER + 2
const val EDIT_EXERCISE_RESULT_OK = Activity.RESULT_FIRST_USER + 3
const val ADD_TRAINING_RESULT_OK = Activity.RESULT_FIRST_USER + 4