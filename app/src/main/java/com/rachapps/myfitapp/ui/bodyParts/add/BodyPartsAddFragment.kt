package com.rachapps.myfitapp.ui.bodyParts.add

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.databinding.FragmentBodyPartsAddBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class BodyPartsAddFragment : Fragment(R.layout.fragment_body_parts_add) {

    private val viewModel: BodyPartsAddViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentBodyPartsAddBinding.bind(view)



        binding.apply {
            fabSaveBodyPart.setOnClickListener {
                var bodyPartName = editTextBodyPartName.text.toString()
                viewModel.onSaveClicked(bodyPartName)
            }

            editTextBodyPartName.setText(viewModel.name)
        }

        collectLatestLifecycleFlow(viewModel.addBodyPartMode){addBodyPartMode ->
            requireActivity().title = if(addBodyPartMode) resources.getString(R.string.add_muscle_group) else resources.getString(R.string.edit_muscle_group)
        }

        collectLatestLifecycleFlow(viewModel.addEditTaskEvent) {
            @Suppress("IMPLICIT_CAST_TO_ANY")
            when (it) {
                 is BodyPartsAddViewModel.BodyPartsAddEvent.NavigateBackWithResult -> {
                    setFragmentResult(
                        "add_edit_request",
                        bundleOf("add_edit_request" to it.result)
                    )

                    findNavController().popBackStack()
                }
                is BodyPartsAddViewModel.BodyPartsAddEvent.ShowInvalidInputMessage -> {
                    Snackbar.make(requireView(), it.msg, Snackbar.LENGTH_LONG).show()
                }
            }.exhaustive
        }
    }

}