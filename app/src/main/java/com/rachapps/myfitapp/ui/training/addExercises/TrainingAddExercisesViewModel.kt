package com.rachapps.myfitapp.ui.training.addExercises

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class TrainingAddExercisesViewModel @Inject constructor(
    private val trainingRepository: TrainingRepository,
    private val state: SavedStateHandle,
) : ViewModel() {

    private val TAG = "TrainingAddViewModel"

    val training = state.get<Training>("training")!!

    val exercises = trainingRepository.getAllExercisesWithSetNoByTrainingId(training.id)

    //Channel with actions
    private val addEditTrainingChannel = Channel<TrainingAddExercisesEvent>()
    val addEditTrainingEvent = addEditTrainingChannel.receiveAsFlow()


    fun onSaveTrainingClicked() {
        viewModelScope.launch {
            addEditTrainingChannel.send(TrainingAddExercisesEvent.NavigateToSortSetsList(training))
        }
    }

    fun onAddSetClicked(exercise: ExerciseWithSetNo) {
        viewModelScope.launch {
            exercise.exercise?.let {
                Log.d(TAG, "onAddSetClicked: ${getTime()}")
                trainingRepository.saveSet(SetEntity(training.id, exercise.exercise.id, getTime()))
            }
        }
    }

    //need unique number which is greater than previous
    // "/10" - no one is able to click twice in 10 millisecond
    // "%1000000000" - need last digits
    private fun getTime() : Int{
        return ((System.currentTimeMillis().toDouble() / 10) % 1000000000).toInt()
    }

    fun onRemoveSetClicked(exercise: ExerciseWithSetNo) {
        viewModelScope.launch {
            exercise.exercise?.let {
                trainingRepository.deleteLastSetByTrainingIDAndExerciseId(training.id, exercise.exercise.id)
            }
        }
    }

    sealed class TrainingAddExercisesEvent {
        data class NavigateBackWithResult(val result: Int) : TrainingAddExercisesEvent()
        data class NavigateToSortSetsList(val training: Training) : TrainingAddExercisesEvent()
    }
}
