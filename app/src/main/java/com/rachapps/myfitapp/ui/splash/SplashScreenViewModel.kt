package com.rachapps.myfitapp.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.PreferencesManager
import com.rachapps.myfitapp.data.repositories.ExerciseRepository
import com.rachapps.myfitapp.data.useCases.FillDbWithInitialDataUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModel @Inject constructor(
    val exerciseRepository: ExerciseRepository,
    val preferencesManager: PreferencesManager
) :
    ViewModel() {

    private val TAG = "SplashScreenViewModel"

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()

    private val preferencesFlow = preferencesManager.preferencesFlow

    init {
        viewModelScope.launch {
            preferencesFlow.collect {
                if (!it.dataDownloaded) {
                    FillDbWithInitialDataUseCase(this,exerciseRepository,preferencesManager).execute()
                    _isLoading.value = false
                } else {
                    _isLoading.value = false
                }
            }
        }
    }



}