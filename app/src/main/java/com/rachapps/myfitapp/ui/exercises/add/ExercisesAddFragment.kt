package com.rachapps.myfitapp.ui.exercises.add

import android.os.Bundle
import android.util.Log

import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult

import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope

import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.databinding.FragmentExercisesAddBinding
import com.rachapps.myfitapp.ui.bodyParts.add.BodyPartsSpinnerAdapter
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ExercisesAddFragment : Fragment(R.layout.fragment_exercises_add) {

    private val viewModel: ExercisesAddViewModel by viewModels()

    private val TAG = "ExercisesAddFragment"

    private var bodyPartsList: List<BodyPart> = ArrayList()

    private lateinit var binding: FragmentExercisesAddBinding

    override fun onResume() {
        super.onResume()
        requireActivity().title = "Add exercise"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentExercisesAddBinding.bind(view)

        val adapter = BodyPartsSpinnerAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            bodyPartsList
        )

        //set up spinner
        collectLatestLifecycleFlow(viewModel.bodyParts) { bodyParts ->
            adapter.addAll(bodyParts)
            adapter.notifyDataSetChanged()
            //setBodyPartSpinnerPositionByBodyPartId(viewModel.bodyPartId)
            Log.d(TAG, "BodyPartsCount ${bodyParts.size}")
            binding.spinnerBodyPart.setSelection(
                setBodyPartSpinnerPositionByBodyPartId(viewModel.bodyPartId)
            )
        }

        //collect events from viewModel
        collectLatestLifecycleFlow(viewModel.addEditExerciseEvent) {
            when (it) {
                is ExercisesAddViewModel.ExerciseAddEvent.ShowInvalidInputMessage -> {
                    Snackbar.make(requireView(), it.msg, Snackbar.LENGTH_LONG).show()
                }
                is ExercisesAddViewModel.ExerciseAddEvent.NavigateBackWithResult -> {
                    setFragmentResult(
                        "add_edit_request",
                        bundleOf("add_edit_request" to it.result)
                    )
                    findNavController().popBackStack()
                }
                is ExercisesAddViewModel.ExerciseAddEvent.ShowAddMusclePartsFirstMessage -> {
                    setViewElementsReadOnly(binding)
                    val snackBar = Snackbar.make(requireView(), it.msg, Snackbar.LENGTH_LONG)
                    snackBar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            super.onDismissed(transientBottomBar, event)
                            Log.d(TAG, "onDismissed: ")
                            findNavController().popBackStack()
                        }
                    })
                    snackBar.show()
                }
            }.exhaustive
        }

        //set up title on start
        lifecycle.coroutineScope.launchWhenStarted {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.addExerciseMode.collect { addExerciseMode ->
                    requireActivity().title =
                        if (addExerciseMode) "Add exercise" else "Edit exercise"
                }
            }
        }

        //set up view on start
        binding.apply {
            spinnerBodyPart.adapter = adapter
            fabSaveBodyPart.setOnClickListener {
                val name: String = editTextExerciseName.text.toString()
                val bodyPart: BodyPart = spinnerBodyPart.selectedItem as BodyPart
                viewModel.onSaveClicked(name, bodyPart)
            }
            editTextExerciseName.setText(viewModel.exerciseName)
        }
    }

    private fun setBodyPartSpinnerPositionByBodyPartId(id: Int): Int {
        var key = 0
        loop@ for ((i, item) in bodyPartsList.withIndex()) {
            if (item.id == id) {
                key = i
                break@loop
            }
        }
        return key
    }

    private fun setViewElementsReadOnly(binding: FragmentExercisesAddBinding) {
        binding.apply {
            fabSaveBodyPart.setOnClickListener(null)
            editTextExerciseName.setTextIsSelectable(false)
            spinnerBodyPart.isClickable = false
        }
    }
}