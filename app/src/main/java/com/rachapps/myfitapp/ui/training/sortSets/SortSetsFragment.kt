package com.rachapps.myfitapp.ui.training.sortSets

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.databinding.FragmentSortSetsBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.job
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

@AndroidEntryPoint
class SortSetsFragment : Fragment(), SortSetsAdapter.OnCheckBoxClickedListener {

    val viewModel: SortSetsViewModel by viewModels()

    lateinit var binding: FragmentSortSetsBinding

    @Inject
    lateinit var sortSetAdapter: SortSetsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sort_sets, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentSortSetsBinding.bind(view)

        binding.apply {
            recyclerViewSets.apply {
                adapter = sortSetAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }

            fabSaveTraining.setOnClickListener {
                findNavController().navigate(SortSetsFragmentDirections.actionSortSetsFragmentToNavGraph())
            }
        }

        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            private var dragFrom = -1
            private var dragTo = -1

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {

                var fromPosition = viewHolder.adapterPosition
                var toPosition = target.adapterPosition

                if (dragFrom == -1) {
                    dragFrom = fromPosition
                }

                dragTo = toPosition
                //pierwszy jest header, wiec nie mozemy nad niego przeciągnąć
                if(toPosition == 0) toPosition = 1

                sortSetAdapter.onItemMoved(fromPosition, toPosition)
                return true
            }



            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                var item = sortSetAdapter.getItems()[viewHolder.adapterPosition]
                when (item){
                    is SortSetsAdapter.DataItem.Row -> {
                        viewModel.onSetSwiped(item.returnSet())
                    }else -> {

                    }
                }
            }

            override fun clearView(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ) {
                super.clearView(recyclerView, viewHolder)

                if (dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
                    viewModel.onSetMoved(dragFrom, dragTo)
                }
                dragFrom = -1
                dragTo = -1
            }

            override fun getMovementFlags(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                return when(viewHolder){
                    is SortSetsAdapter.HeaderViewHolder -> 0
                    else ->{
                        super.getMovementFlags(recyclerView, viewHolder)
                    }
                }
            }
        }).attachToRecyclerView(binding.recyclerViewSets)


        collectLatestLifecycleFlow(viewModel.sortSetsEvent) { event ->
            when (event) {
                is SortSetsViewModel.SortSetsEvents.ShowUndoDeleteSetMessage -> {
                    updateList()

                    Snackbar.make(requireView(), "Set has been deleted", Snackbar.LENGTH_LONG)
                        .setAction("UNDO") { viewModel.onUndoDeleteClick(event.SetAndExercise) }
                        .show()
                }
                is SortSetsViewModel.SortSetsEvents.NavigateToTrainingsList -> {

                }
                is SortSetsViewModel.SortSetsEvents.OnUndoDelete -> {
                    updateList()
                }
                is SortSetsViewModel.SortSetsEvents.OnOrderChanged -> {
                    updateList()
                }
            }.exhaustive

        }
        updateList()
    }

    private fun updateList() {
        collectLatestLifecycleFlow(viewModel.sets){ setAndExerciseList->
            coroutineContext.job.cancel()
            if (setAndExerciseList.isEmpty()) {
                binding.textViewNoDataMessage.visibility = View.VISIBLE
                binding.recyclerViewSets.visibility = View.GONE
            } else {
                sortSetAdapter.setItems(setAndExerciseList.toMutableList())
                sortSetAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onCheckBoxClicked(set: SetAndExercise, checked: Boolean) {
        viewModel.onWarmUpCheckBoxClicked(set, checked)
    }
}