package com.rachapps.myfitapp.ui.exercises.add

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.dao.BodyPartDao
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.dao.ExerciseDao
import com.rachapps.myfitapp.data.repositories.ITrainingRepository
import com.rachapps.myfitapp.ui.ADD_EXERCISE_RESULT_OK
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val TAG = "ExercisesAddViewModel"

@HiltViewModel
class ExercisesAddViewModel @Inject constructor(
    val bodyPartDao: BodyPartDao,
    val exerciseDao: ExerciseDao,
    private val state: SavedStateHandle
) : ViewModel() {


    private val addEditExerciseChannel = Channel<ExerciseAddEvent>()
    val addEditExerciseEvent = addEditExerciseChannel.receiveAsFlow()


    val bodyParts = bodyPartDao.getAll("").transform { value ->
        if (value.isEmpty()) {
            Log.d(TAG, "Empty value: Do not emit")
            addEditExerciseChannel.send(ExerciseAddEvent.ShowAddMusclePartsFirstMessage("You need to add muscle group first"))
        } else {
            Log.d(TAG, "Value is correct: Emit")
            emit(value)
        }
    }


    val exercise = state.get<Exercise>("exercise")

    val addExerciseMode = MutableStateFlow(exercise == null)

    var exerciseName = state.get<String>("exerciseName") ?: exercise?.name ?: ""
        set(value) {
            field = value
            state.set("exerciseName", value)
        }

    var bodyPartId = state.get<Int>("bodyPartId") ?: exercise?.bodyPartId ?: 0
        set(value) {
            field = value
            state.set("bodyPartId", value)
        }


    fun onSaveClicked(name: String, bodyPart: BodyPart) {
        viewModelScope.launch {
            if (name.isEmpty()) {
                addEditExerciseChannel.send(ExerciseAddEvent.ShowInvalidInputMessage("Name cannot be empty"))
            } else {
                if (exercise != null) {
                    exerciseDao.update(Exercise(name, bodyPart.id, exercise.id))
                } else {
                    exerciseDao.insert(Exercise(name, bodyPart.id))
                }

                addEditExerciseChannel.send(
                    ExerciseAddEvent.NavigateBackWithResult(
                        ADD_EXERCISE_RESULT_OK
                    )
                )
            }
        }

    }

    sealed class ExerciseAddEvent {
        data class ShowInvalidInputMessage(val msg: String) : ExerciseAddEvent()
        data class NavigateBackWithResult(val result: Int) : ExerciseAddEvent()
        data class ShowAddMusclePartsFirstMessage(val msg: String) : ExerciseAddEvent()
    }

}