package com.rachapps.myfitapp.ui.exercises.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.dao.ExerciseDao
import com.rachapps.myfitapp.data.repositories.ExerciseRepository
import com.rachapps.myfitapp.data.repositories.IExerciseRepository
import com.rachapps.myfitapp.data.repositories.ITrainingRepository
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ExercisesViewModel @Inject constructor(
    val exerciseDao: ExerciseDao,
    val trainingRepository: ITrainingRepository
) : ViewModel() {

    private val searchQuery = MutableStateFlow("")

    val bodyPartsFlow = searchQuery.flatMapLatest { query ->
        exerciseDao.getAllByNameSearched(query)
    }

    private val exerciseEventChannel = Channel<ExerciseEvents>()
    val exerciseEvent = exerciseEventChannel.receiveAsFlow()

    fun onExerciseSwiped(exercise: Exercise) {
        viewModelScope.launch {
            val isExerciseUsedInSets = trainingRepository.countSetsByExerciseId(exercise.id) > 0
            if (isExerciseUsedInSets) {
                bodyPartsFlow.last()
            } else {
                exerciseDao.delete(exercise)
                exerciseEventChannel.send(ExerciseEvents.ShowUndoDeleteExerciseMessage(exercise))
            }

        }
    }

    fun onUndoDeleteClick(exercise: Exercise) {
        viewModelScope.launch {
            exerciseDao.insert(exercise)
        }
    }


    fun onItemSelected(exercise: Exercise) {
        viewModelScope.launch {
            exerciseEventChannel.send(ExerciseEvents.NavigateToEditExercise(exercise))
        }
    }

    fun onTextSearchQueryChange(search: String) {
        searchQuery.value = search
    }

    sealed class ExerciseEvents {
        data class NavigateToEditExercise(val exercise: Exercise) : ExerciseEvents()
        data class ShowUndoDeleteExerciseMessage(val exercise: Exercise) : ExerciseEvents()
    }

}