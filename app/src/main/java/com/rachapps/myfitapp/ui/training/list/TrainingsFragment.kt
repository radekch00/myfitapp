package com.rachapps.myfitapp.ui.training.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.databinding.FragmentTrainingsBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class TrainingsFragment : Fragment(R.layout.fragment_trainings),
    TrainingsAdapter.OnItemClickListener {


    val viewModel: TrainingsViewModel by viewModels()

    @Inject
    lateinit var trainingsAdapter: TrainingsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().title = "Trainings"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentTrainingsBinding.bind(view)

        binding.apply {
            fabAddTraining.setOnClickListener {
                findNavController().navigate(
                    TrainingsFragmentDirections.actionTrainingsFragmentToAddTrainingFragment()
                )
            }

            recyclerViewTrainings.apply {
                adapter = trainingsAdapter
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
            }
        }

        collectLatestLifecycleFlow(viewModel.trainings) {
            trainingsAdapter.submitList(it)
        }


        collectLatestLifecycleFlow(viewModel.trainingsEvent) { event ->
            when (event) {
                is TrainingsViewModel.TrainingsEvent.OnTrainingSelected -> {
                    findNavController().navigate(
                        TrainingsFragmentDirections.actionTrainingsFragmentToWorkout(
                            event.training
                        )
                    )
                }
                is TrainingsViewModel.TrainingsEvent.OnEditTrainingSelected -> {
                    findNavController().navigate(

                        TrainingsFragmentDirections.actionTrainingsFragmentToAddTrainingFragment(
                            event.training
                        )
                    )
                }
            }.exhaustive
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_trainings, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_exercises -> {
                findNavController().navigate(TrainingsFragmentDirections.actionTrainingsFragmentToExercisesFragment())
                true
            }
            R.id.menu_item_body_parts -> {
                findNavController().navigate(TrainingsFragmentDirections.actionTrainingsFragmentToBodyPartsFragment())
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onItemClick(training: Training, item: TrainingsAdapter.ItemClickType) {
        when (item) {
            TrainingsAdapter.ItemClickType.ROOT -> {
                viewModel.onTrainingSelected(training)
            }
            TrainingsAdapter.ItemClickType.EDIT_BUTTON -> {
                viewModel.onEditTrainingSelected(training)
            }
        }.exhaustive

    }
}