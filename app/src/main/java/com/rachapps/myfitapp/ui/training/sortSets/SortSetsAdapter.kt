package com.rachapps.myfitapp.ui.training.sortSets


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.databinding.ItemSetsSortBinding
import com.rachapps.myfitapp.databinding.ItemSetsSortHeaderBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


private val ITEM_VIEW_TYPE_HEADER = 0
private val ITEM_VIEW_TYPE_ITEM = 1

class SortSetsAdapter @Inject constructor(private val listener : OnCheckBoxClickedListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "SortSetsAdapter"

    private var items = listOf<SortSetsAdapter.DataItem>()

    fun setItems(sets : MutableList<SetAndExercise>){
        addHeaderAndSubmitList(sets)
    }


    private fun addHeaderAndSubmitList(sets : MutableList<SetAndExercise>){
        items = when(sets){
            null -> listOf(DataItem.Header)
            else-> listOf(DataItem.Header) + sets.map{DataItem.Row(it)}
        }
    }

    fun getItems() = items

    private fun getItem(position : Int): SetAndExercise{
        val s = items[position] as DataItem.Row
        return s.returnSet()
    }


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when(viewType){
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder.from(parent)
            ITEM_VIEW_TYPE_ITEM -> {
                val binding = ItemSetsSortBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                SortSetsViewHolder(binding)
            }
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder){
            is SortSetsViewHolder -> holder.bind(getItem(position))
        }
    }

    fun onItemMoved(from : Int, to : Int){
        notifyItemMoved(from, to)
    }

    override fun getItemViewType(position: Int): Int {
        return when(position){
            0 -> ITEM_VIEW_TYPE_HEADER
            else -> ITEM_VIEW_TYPE_ITEM
        }
    }


    inner class SortSetsViewHolder(private val binding: ItemSetsSortBinding) :
        RecyclerView.ViewHolder(binding.root) {


        init{
            binding.checkBoxWarmUp.apply {
                setOnCheckedChangeListener { _, isChecked ->
                    Log.d(TAG, "onCheckedChanged: $isChecked")
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        getItem(position)?.let { listener.onCheckBoxClicked(it, isChecked) }
                    }
                }
            }
        }

        fun bind(set: SetAndExercise) {
            binding.apply {
                textViewName.text = set.exercise.name
                checkBoxWarmUp.isChecked = set.set.warmUp
            }
        }

    }

     class HeaderViewHolder(private val binding: ItemSetsSortHeaderBinding) : RecyclerView.ViewHolder(binding.root){
        companion object{
            fun from(parent: ViewGroup) : HeaderViewHolder{
                val binding = ItemSetsSortHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return HeaderViewHolder(binding)
            }
        }
    }

    interface OnCheckBoxClickedListener{
        fun onCheckBoxClicked(set: SetAndExercise, checked : Boolean)
    }

    sealed class DataItem{
        object Header : DataItem()
        class Row(public val set: SetAndExercise) : DataItem(){
            public fun returnSet() = set
        }
    }
}