package com.rachapps.myfitapp.ui.exercises.list

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.rachapps.myfitapp.R
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.databinding.FragmentExercisesBinding
import com.rachapps.myfitapp.util.collectLatestLifecycleFlow
import com.rachapps.myfitapp.util.exhaustive
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ExercisesFragment : Fragment(R.layout.fragment_exercises),
    ExerciseAdapter.OnItemClickListener {

    private val viewModel: ExercisesViewModel by viewModels()

    private val TAG = "ExercisesFragment"

    @Inject
    lateinit var exerciseAdapter: ExerciseAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().title = "Exercises"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentExercisesBinding.bind(view)

        binding.apply {
            fabAddExercise.apply {
                setOnClickListener {
                    findNavController().navigate(
                        ExercisesFragmentDirections.actionExercisesFragmentToExercisesAddFragment(
                            null
                        )
                    )
                }
            }

            recyclerViewExercises.apply {
                setHasFixedSize(true)
                adapter = exerciseAdapter
                layoutManager = LinearLayoutManager(requireContext())
            }
        }

        collectLatestLifecycleFlow(viewModel.bodyPartsFlow) { exercises ->
            exerciseAdapter.submitList(exercises)
        }

        setFragmentResultListener("add_edit_request") { _, bundle ->
            val result = bundle.getInt("add_edit_result")
            //TODO: Call viewmodel to do smth on added or updated results
            Log.d(TAG, "onAddOrEditExercise")
        }

        collectLatestLifecycleFlow(viewModel.exerciseEvent) { event ->
            when (event) {
                is ExercisesViewModel.ExerciseEvents.NavigateToEditExercise -> {
                    val action =
                        ExercisesFragmentDirections.actionExercisesFragmentToExercisesAddFragment(
                            event.exercise
                        )
                    findNavController().navigate(action)
                }
                is ExercisesViewModel.ExerciseEvents.ShowUndoDeleteExerciseMessage -> {
                    Snackbar.make(requireView(), "Exercise deleted", Snackbar.LENGTH_LONG)
                        .setAction("Undo") { viewModel.onUndoDeleteClick(event.exercise) }
                        .show()
                }
            }.exhaustive
        }


        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val exercise = exerciseAdapter.currentList[viewHolder.adapterPosition]
                viewModel.onExerciseSwiped(exercise)
            }
        }).attachToRecyclerView(binding.recyclerViewExercises)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_exercises, menu)
    val searchView = menu.findItem(R.id.menu_item_search_exercise).actionView as SearchView

    searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
        override fun onQueryTextSubmit(p0: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(p0: String?): Boolean {
            viewModel.onTextSearchQueryChange(p0.orEmpty())
            return true
        }
    })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_body_parts -> {
                findNavController().navigate(ExercisesFragmentDirections.actionExercisesFragmentToBodyPartsFragment())
                true
            }
            R.id.menu_item_trainings ->{
                findNavController().navigate(ExercisesFragmentDirections.actionExercisesFragmentToTrainingsFragment())
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onItemClick(exercise: Exercise) {
        viewModel.onItemSelected(exercise)
    }
}