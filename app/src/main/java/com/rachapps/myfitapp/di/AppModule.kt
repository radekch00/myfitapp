package com.rachapps.myfitapp.di

import android.app.Application
import androidx.fragment.app.Fragment
import androidx.room.Room
import com.rachapps.myfitapp.data.MyFitDatabase
import com.rachapps.myfitapp.data.remote.MyFitAPI
import com.rachapps.myfitapp.data.repositories.ExerciseRepository
import com.rachapps.myfitapp.data.repositories.IExerciseRepository
import com.rachapps.myfitapp.data.repositories.ITrainingRepository
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import com.rachapps.myfitapp.other.Constants.BASE_URL
import com.rachapps.myfitapp.other.Constants.DATABASE_NAME
import com.rachapps.myfitapp.ui.bodyParts.list.BodyPartsAdapter
import com.rachapps.myfitapp.ui.bodyParts.list.BodyPartsFragment
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideDatabase(app : Application) = Room.databaseBuilder(app, MyFitDatabase::class.java, DATABASE_NAME)
        .fallbackToDestructiveMigration()
        .build()

    @ApplicationScope
    @Provides
    @Singleton
    fun provideApplicationScope() = CoroutineScope(SupervisorJob())

    @Provides
    fun provideExerciseDao(myFitDatabase: MyFitDatabase) = myFitDatabase.exerciseDao()

    @Provides
    fun provideBodyPartDao(myFitDatabase: MyFitDatabase) = myFitDatabase.bodyPartDao()

    @Provides
    fun providesTrainingDao(myFitDatabase: MyFitDatabase) = myFitDatabase.trainingDao()

    @Provides
    fun providesSetDao(myFitDatabase: MyFitDatabase) = myFitDatabase.setDao()

    @Singleton
    @Provides
    fun provideMyFitAPI() : MyFitAPI{
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(MyFitAPI::class.java)
    }

    @Singleton
    @Provides
    fun providesITrainingRepository(trainingRepository: TrainingRepository) : ITrainingRepository {
        return trainingRepository
    }

    @Singleton
    @Provides
    fun providesIExerciseRepository(exerciseRepository: ExerciseRepository) : IExerciseRepository {
        return exerciseRepository
    }
}

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope