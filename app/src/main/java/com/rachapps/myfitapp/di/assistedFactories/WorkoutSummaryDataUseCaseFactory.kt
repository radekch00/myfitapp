package com.rachapps.myfitapp.di.assistedFactories

import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.useCases.WorkoutSummaryDataUseCase
import dagger.assisted.AssistedFactory
import kotlinx.coroutines.CoroutineScope

@AssistedFactory
interface WorkoutSummaryDataUseCaseFactory {
    fun create(
        training: Training,
        scope: CoroutineScope,
        listener: WorkoutSummaryDataUseCase.OnSetSummaryListener
    ): WorkoutSummaryDataUseCase
}