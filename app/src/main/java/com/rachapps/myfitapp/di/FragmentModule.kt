package com.rachapps.myfitapp.di

import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.rachapps.myfitapp.ui.bodyParts.list.BodyPartsAdapter
import com.rachapps.myfitapp.ui.exercises.list.ExerciseAdapter
import com.rachapps.myfitapp.ui.training.addExercises.ExercisesInTrainingAdapter
import com.rachapps.myfitapp.ui.training.addExercises.TrainingAddExercisesFragment
import com.rachapps.myfitapp.ui.training.addExercises.TrainingAddExercisesViewModel
import com.rachapps.myfitapp.ui.training.list.TrainingsAdapter
import com.rachapps.myfitapp.ui.training.sortSets.SortSetsAdapter
import com.rachapps.myfitapp.ui.workout.SetsAdapter
import com.rachapps.myfitapp.ui.workout.WorkoutIntroFragment
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import javax.inject.Singleton

@Module
@InstallIn(FragmentComponent::class)
object FragmentModule {

    @Provides
    fun providesWorkOutIntroOnItemClickListener(fragment: Fragment) : SetsAdapter.OnItemClickListener{
        return fragment as WorkoutIntroFragment as SetsAdapter.OnItemClickListener
    }

    @Provides
    fun provideBodyPartsOnItemClickListener(fragment: Fragment): BodyPartsAdapter.OnItemClickListener {
        return fragment as BodyPartsAdapter.OnItemClickListener
    }

    @Provides
    fun providesExerciseOnItemCLickListener(fragment: Fragment) =
        fragment as ExerciseAdapter.OnItemClickListener

    @Provides
    fun providesOnChangeNoOfSetClickListener(fragment: Fragment) =
        fragment as ExercisesInTrainingAdapter.OnChangeNoOfSetClickListener

    @Singleton
    @Provides
    fun provideViewModel(fragment : Fragment) : TrainingAddExercisesViewModel {
        val exercisesViewModel : TrainingAddExercisesViewModel by (fragment as TrainingAddExercisesFragment).viewModels()
        return exercisesViewModel
    }

    @Provides
    fun providesTrainingOnItemClickListener(fragment: Fragment) = fragment as TrainingsAdapter.OnItemClickListener

    @Provides
    fun provideSortSetsOnCheckBoxClickedListener(fragment : Fragment) = fragment as SortSetsAdapter.OnCheckBoxClickedListener
//
//    @Provides
//    fun provideCoroutineScope(viewModel : TrainingAddViewModel) : CoroutineScope{
//        return viewModel.viewModelScope
//    }
}