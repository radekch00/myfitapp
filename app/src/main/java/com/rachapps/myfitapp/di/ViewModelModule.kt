package com.rachapps.myfitapp.di

import com.rachapps.myfitapp.ui.training.addExercises.TrainingAddExercisesViewModel
import com.rachapps.myfitapp.ui.training.addExercises.UpdateAddExercisesFrontData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object ViewModelModule {

    @Provides
    fun providesUpdateFrontEndData(exercisesViewModel : TrainingAddExercisesViewModel) : UpdateAddExercisesFrontData {
        return exercisesViewModel as UpdateAddExercisesFrontData
    }
}