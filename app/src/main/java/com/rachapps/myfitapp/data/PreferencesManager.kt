package com.rachapps.myfitapp.data

import android.content.Context
import android.util.Log
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences

import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import java.lang.Boolean.FALSE
import javax.inject.Inject
import javax.inject.Singleton

val Context.dataStore by preferencesDataStore("my_data_store")

private const val TAG = "PreferencesManager"

@Singleton
class PreferencesManager @Inject constructor(@ApplicationContext context: Context) {

    private val dataStore = context.dataStore

    val preferencesFlow = dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
                Log.d(TAG, "Error reading preferences: ", exception)
            } else {
                throw exception
            }
        }
        .map {
            FilterPreferences(
                it[PreferencesKeys.HIDE_NOT_USED] ?: FALSE,
                it[PreferencesKeys.DATA_WAS_DOWNLOADED] ?: FALSE
            )
        }

    private object PreferencesKeys {
        val HIDE_NOT_USED = booleanPreferencesKey("hide_not_used")
        val DATA_WAS_DOWNLOADED = booleanPreferencesKey("data_was_downloaded")
    }

    suspend fun updateHideNotUsed(hideNotUsed: Boolean) {
        dataStore.edit {
            it[PreferencesKeys.HIDE_NOT_USED] = hideNotUsed
        }
    }

    suspend fun updateDataWasDownloaded(dataWasDownloaded: Boolean) {
        dataStore.edit {
            it[PreferencesKeys.DATA_WAS_DOWNLOADED] = dataWasDownloaded
        }
    }
}

data class FilterPreferences(val hideNotUsed: Boolean, val dataDownloaded: Boolean)