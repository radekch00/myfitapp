package com.rachapps.myfitapp.data.remote.responses

import com.google.gson.annotations.SerializedName

data class ExerciseResponse (

    @SerializedName("name") val name : String,
    @SerializedName("id") val id : Int,
    @SerializedName("bodyPartId") val bodyPartId : Int
)