package com.rachapps.myfitapp.data.useCases

import com.rachapps.myfitapp.data.entities.Training
import java.time.LocalDate

class CreateOrUpdateTrainingEntityUseCase constructor(
    private val savedTraining : Training?,
    private val newTraining: Training
) {
    fun execute() =
        createTrainingEntity(savedTraining, newTraining.date.toString(), newTraining.title, newTraining.description)


    private fun createTrainingEntity(
        savedTraining : Training?,
        date: String,
        name: String,
        description: String
    ): Training {
        var training = savedTraining?.let {
            updateTrainingEntity(savedTraining, date, name, description)
        } ?: run {
            createNewTrainingEntity(date, name, description)
        }
        return training
    }

    private fun createNewTrainingEntity(
        date: String,
        name: String,
        description: String
    ) : Training {
        return Training(LocalDate.parse(date), name, description)
    }

    private fun updateTrainingEntity(
        savedTraining: Training,
        date: String,
        name: String,
        description: String
    ) : Training {
        return savedTraining.copy(
            date = LocalDate.parse(date),
            title = name,
            description = description
        )
    }
}