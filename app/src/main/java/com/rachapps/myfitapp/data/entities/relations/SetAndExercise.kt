package com.rachapps.myfitapp.data.entities.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.entities.SetEntity

data class SetAndExercise(
    @Embedded val set : SetEntity,
    @Relation(
        parentColumn = "exerciseId",
        entityColumn = "id"
    )
    val exercise: Exercise
)