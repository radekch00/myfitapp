package com.rachapps.myfitapp.data.entities.relations

import android.os.Parcelable
import androidx.room.Embedded
import com.rachapps.myfitapp.data.entities.BodyPart
import kotlinx.parcelize.Parcelize

@Parcelize
 data class BodyPartsWithExercisesCount constructor(
    @Embedded
    var bodyPart : BodyPart,
    var exercisesCount : Int = 0
 ) : Parcelable{

 }