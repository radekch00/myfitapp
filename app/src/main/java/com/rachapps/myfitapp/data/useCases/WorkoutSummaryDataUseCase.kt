package com.rachapps.myfitapp.data.useCases

import android.util.Log
import com.rachapps.myfitapp.data.dto.ExerciseRecordDTO
import com.rachapps.myfitapp.data.dto.WorkoutSummaryDTO
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

//prepare workout summary data
class WorkoutSummaryDataUseCase @AssistedInject constructor(
    private val trainingRepository: TrainingRepository,
    @Assisted private val training: Training,
    @Assisted private val scope: CoroutineScope,
    @Assisted private val listener : WorkoutSummaryDataUseCase.OnSetSummaryListener
) {

    private val TAG = "WorkoutSummaryUseCase"

    fun execute() {

        val allSetsInTraining = trainingRepository.getSetAndExercise(trainingId = training.id)

        scope.launch(Dispatchers.IO) {
            allSetsInTraining.collect { setsAndExercises ->
                var summary = currentWorkoutSummary(setsAndExercises)

                val records = mutableListOf<ExerciseRecordDTO>()

                var exercisesIdsList = mutableListOf<Int>()
                setsAndExercises.forEach { setAndExercise ->
                    exercisesIdsList.add(setAndExercise.exercise.id)
                }

                val uniqueExercisesIdsList = exercisesIdsList.distinct()
                uniqueExercisesIdsList.forEach { exerciseId ->
                    var exerciseRecord = trainingRepository.selectMaxWeight(
                        exerciseId = exerciseId,
                        notTrainingId = training.id
                    )
                    var currentWorkoutRecord = trainingRepository.selectMaxWeight(
                        exerciseId = exerciseId,
                        trainingId = training.id
                    )
                    Log.d(TAG, "Record: $exerciseRecord, This workout: $currentWorkoutRecord")
                    records.add(ExerciseRecordDTO(exerciseId, currentWorkoutRecord, exerciseRecord))

                }

                summary.records = records
                listener.onSetSummaryData(summary)
            }
        }
    }


    private fun currentWorkoutSummary(setEntity: List<SetAndExercise>): WorkoutSummaryDTO {
        var setNo = 0
        var warmUpNo = 0
        var workingSetsNo = 0
        var totalWeight = 0.0
        var warmUpWeight = 0.0
        var workingSetsWeight = 0.0

        setEntity.forEach { setAndExercise ->
            var set = setAndExercise.set
            setNo++
            totalWeight += set.weight
            if (set.warmUp) {
                warmUpNo++
                warmUpWeight += set.weight
            } else {
                workingSetsNo++
                workingSetsWeight += set.weight
            }
        }

        return WorkoutSummaryDTO(
            training,
            setNo,
            warmUpNo,
            workingSetsNo,
            totalWeight,
            warmUpWeight,
            workingSetsWeight
        )
    }

    interface OnSetSummaryListener {
        fun onSetSummaryData(workoutSummaryDTO : WorkoutSummaryDTO)
    }
}