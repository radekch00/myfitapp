package com.rachapps.myfitapp.data.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Entity(tableName = "exercise")
@Parcelize
data class Exercise(
    val name: String,
    val bodyPartId : Int,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
) : Parcelable{

}