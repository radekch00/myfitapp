package com.rachapps.myfitapp.data.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rachapps.myfitapp.data.enums.TrainingStatus
import kotlinx.parcelize.Parcelize
import java.time.LocalDate
import java.util.*

@Parcelize
@Entity(tableName = "training")
data class Training(
    val date: LocalDate?,
    val title: String = "Training",
    val description: String = "",
    val status: TrainingStatus = TrainingStatus.READY,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
) : Parcelable {

    fun isValid(): Boolean {
        return isTitleValid()
    }

    private fun isTitleValid() : Boolean {
        return !title.isNullOrBlank()
    }


}
