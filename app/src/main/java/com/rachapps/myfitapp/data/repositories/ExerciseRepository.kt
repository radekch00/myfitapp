package com.rachapps.myfitapp.data.repositories

import com.rachapps.myfitapp.data.dao.BodyPartDao
import com.rachapps.myfitapp.data.dao.ExerciseDao
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.remote.MyFitAPI
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import com.rachapps.myfitapp.other.Resource
import retrofit2.Response
import javax.inject.Inject


class ExerciseRepository @Inject constructor(
    private val exerciseDao: ExerciseDao,
    private val bodyPartDao: BodyPartDao,
    private val myFitApi: MyFitAPI
) : IExerciseRepository {


    override suspend fun getRemoteBodyParts(): Resource<BodyPartsResponse> {
        return try {
            val response = myFitApi.getBodyParts()
            parseResponse(response)
        } catch (e: Exception) {
            Resource.error("Couldn't reach the server", null)
        }
    }

    override suspend fun getRemoteExercises(): Resource<ExercisesResponse> {
        return try {
            val response = myFitApi.getExercises()
            parseResponse(response)
        } catch (e: Exception) {
            Resource.error("Couldn't reach the server", null)
        }
    }

    private fun <T> parseResponse(response: Response<T>): Resource<T> {
        return if (response.isSuccessful) {
            response.body()?.let {
                return@let Resource.success(it)
            } ?: Resource.error("No data", null)
        } else {
            Resource.error("Unknown error", null)
        }
    }

    override suspend fun saveExercise(exercise: Exercise): Long {
        return if(exercise.id == 0){
            exerciseDao.insert(exercise)
        }else{
            exerciseDao.update(exercise)
            exercise.id.toLong()
        }
    }

    override suspend fun saveBodyPart(bodyPart: BodyPart): Long {
        return if(bodyPart.id == 0){
            bodyPartDao.insert(bodyPart)
        }else{
            bodyPartDao.update(bodyPart)
            bodyPart.id.toLong()
        }
    }
}