package com.rachapps.myfitapp.data.repositories

import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import com.rachapps.myfitapp.other.Resource

interface IExerciseRepository {
    suspend fun saveExercise(exercise : Exercise) : Long

    suspend fun saveBodyPart(bodyPart: BodyPart) : Long

    suspend fun getRemoteBodyParts() : Resource<BodyPartsResponse>

    suspend fun getRemoteExercises() : Resource<ExercisesResponse>
}