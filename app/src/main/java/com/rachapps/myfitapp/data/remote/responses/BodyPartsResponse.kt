package com.rachapps.myfitapp.data.remote.responses

data class BodyPartsResponse(
    val bodyParts : List<BodyPartResponse>,
    val success : Int,
    val message : String
)
