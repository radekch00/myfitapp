package com.rachapps.myfitapp.data.dao

import androidx.room.*
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import kotlinx.coroutines.flow.Flow
import androidx.room.Update




@Dao
interface SetDao {

    @Insert
    suspend fun insert(setEntity: SetEntity) : Long

    @Update
    suspend fun update(setEntity: SetEntity)

    @Update
    suspend fun updateSets(vararg setItem: SetEntity?)

    @Delete
    suspend fun delete(setEntity: SetEntity)

    @Query("Select * from setTable")
    fun selectAll() : Flow<List<SetEntity>>

    @Query("Delete FROM setTable WHERE id IN (SELECT id FROM setTable WHERE trainingId = :trainingId AND exerciseId = :exerciseId ORDER BY `order` DESC limit 1)")
    suspend fun deleteLastSetByTrainingIDAndExerciseId(trainingId : Int, exerciseId : Int)

    @Transaction
    @Query("Select * from setTable WHERE trainingId = :trainingId And `order` >= :order ORDER by `order`, id LIMIT :limit")
    fun selectSetAndExerciseByTrainingId(trainingId: Int, limit: Int, order : Int) : Flow<List<SetAndExercise>>

    @Transaction
    @Query("Select * from setTable WHERE id = :setId AND `order` >= :order ORDER by `order`, id LIMIT 1000")
    fun selectSetAndExerciseBySetId(setId: Int, order: Int) : Flow<List<SetAndExercise>>

    @Transaction
    @Query("Select * from setTable WHERE trainingId = :trainingId ORDER by `order`, id LIMIT 1")
    fun selectFirstSetAndExerciseByTrainingId(trainingId: Int) : Flow<List<SetAndExercise>>

    @Query("Select * from setTable WHERE trainingId = :trainingId ORDER by `order`, id")
    fun selectSetByTrainingId(trainingId : Int) : Flow<List<SetEntity>>

    @Query("Select * FROM setTable WHERE exerciseId = :exerciseId Order by weight desc, repeats desc Limit 1")
    fun selectMaxWeightByExerciseId(exerciseId: Int) : SetAndExercise

    @Query("Select * FROM setTable WHERE exerciseId = :exerciseId AND trainingId = :trainingId  Order by weight desc, repeats desc Limit 1")
    fun selectMaxWeightByExerciseIdAndTrainingId(exerciseId: Int, trainingId:Int) : SetAndExercise

    @Query("Select * FROM setTable WHERE exerciseId = :exerciseId AND trainingId != :trainingId  Order by weight desc, repeats desc Limit 1")
    fun selectMaxWeightByExerciseIdAndNotTrainingId(exerciseId: Int, trainingId:Int) : SetAndExercise

    @Query("Select count(*) as c from setTable WHERE exerciseId = :exerciseId")
    suspend fun countSetsByExerciseId(exerciseId: Int) : Int

}