package com.rachapps.myfitapp.data.dao

import androidx.room.*
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.relations.BodyPartWithExercises
import com.rachapps.myfitapp.data.entities.relations.BodyPartsWithExercisesCount
import kotlinx.coroutines.flow.Flow


@Dao
interface BodyPartDao {

    @Query("Select * from bodyPart WHERE name LIKE '%'|| :searchQuery ||'%' ORDER BY name")
    fun getAll(searchQuery: String): Flow<List<BodyPart>>

    @Query("Select P.*, count(E.id) AS exercisesCount from bodyPart P LEFT JOIN exercise E ON P.id = E.bodyPartId  WHERE p.name LIKE '%'|| :searchQuery ||'%' GROUP BY P.id ORDER BY name")
    fun getAllWithExercisesCountByName(searchQuery: String): Flow<List<BodyPartsWithExercisesCount>>

    @Query("Select P.*, count(E.id) AS exercisesCount from bodyPart P LEFT JOIN exercise E ON P.id = E.bodyPartId  WHERE p.id = :bodyPartId GROUP BY P.id ORDER BY name")
    fun getAllWithExercisesCountByBodyPartId(bodyPartId: Int): Flow<List<BodyPartsWithExercisesCount>>

    @Transaction
    @Query("Select * FROM bodyPart WHERE id = :id")
    fun getBodyPartWithExercisesById(id : Int) : Flow<List<BodyPartWithExercises>>

    @Insert
    suspend fun insert(bodyPart: BodyPart):Long

    @Update
    suspend fun update(bodyPart: BodyPart)

    @Delete
    suspend fun delete(bodyPart: BodyPart)
}