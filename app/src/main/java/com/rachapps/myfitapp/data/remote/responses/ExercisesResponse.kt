package com.rachapps.myfitapp.data.remote.responses

import com.google.gson.annotations.SerializedName

class ExercisesResponse (
    @SerializedName("exercises") val exercises : List<ExerciseResponse>,
    @SerializedName("success") val success : Int,
    @SerializedName("message") val message : String
)