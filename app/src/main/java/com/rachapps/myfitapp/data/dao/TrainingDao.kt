package com.rachapps.myfitapp.data.dao

import androidx.room.*
import com.rachapps.myfitapp.data.entities.Training
import kotlinx.coroutines.flow.Flow

@Dao
interface TrainingDao {


    @Query("Select * from training order by date")
    fun selectAll(): Flow<List<Training>>

    @Query("Select * from training WHERE id = :id")
    fun selectTrainingById(id: Int): Flow<List<Training>>

    @Insert
    suspend fun insert(training: Training): Long

    @Delete
    suspend fun delete(training: Training)

    @Update
    suspend fun update(training: Training)
}