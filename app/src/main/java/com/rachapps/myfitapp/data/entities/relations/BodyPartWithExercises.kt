package com.rachapps.myfitapp.data.entities.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.Exercise


data class BodyPartWithExercises(
  @Embedded val bodyPart : BodyPart,
  @Relation(
    parentColumn = "id",
    entityColumn = "bodyPartId"
  )
  val exercise: Exercise
)