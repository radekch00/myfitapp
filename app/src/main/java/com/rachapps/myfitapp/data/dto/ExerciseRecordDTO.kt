package com.rachapps.myfitapp.data.dto

import com.rachapps.myfitapp.data.entities.relations.SetAndExercise

data class ExerciseRecordDTO(val exerciseId: Int, val currentWeight: SetAndExercise, val maxWeight: SetAndExercise)
