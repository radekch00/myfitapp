package com.rachapps.myfitapp.data.dao

import android.util.Log
import androidx.room.*
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import kotlinx.coroutines.flow.Flow

private const val TAG = "ExerciseDao"

@Dao
interface ExerciseDao {

    @Query("Select * from exercise")
    fun getAll(): Flow<List<Exercise>>

    fun getAllExercisesWithSetNo(trainingId : Int? = null) : Flow<List<ExerciseWithSetNo>> {
        return if(trainingId == null) {
            Log.d(TAG, "getAllExercisesWithSetNo: ")
            getAllExercisesWithSetNoForEmptyTrainingId()
        }else{
            Log.d(TAG, "getAllExercisesWithSetNo: ")
            //getAllExercisesWithSetNoForEmptyTrainingId()
            getAllExercisesWithSetNoForTrainingId(trainingId)
        }
    }


    @Query("Select e.*, 0 AS setNo from exercise e")
    fun getAllExercisesWithSetNoForEmptyTrainingId(): Flow<List<ExerciseWithSetNo>>

    @Query("Select e.*, ifnull(t.setNo, 0) AS setNo from exercise e LEFT JOIN (SELECT exerciseId, count(*) as setNo FROM setTable WHERE trainingId = :trainingId Group by exerciseId) t ON t.exerciseId = e.id  GROUP BY e.id")
    fun getAllExercisesWithSetNoForTrainingId(trainingId: Int): Flow<List<ExerciseWithSetNo>>


    @Query("Select * from exercise WHERE name LIKE '%' || :search || '%' order by name")
    fun getAllByNameSearched(search : String): Flow<List<Exercise>>

    @Insert
    suspend fun insert(exercise: Exercise) : Long

    @Update
    suspend fun update(exercise: Exercise)

    @Delete
    suspend fun delete(exercise: Exercise)
}