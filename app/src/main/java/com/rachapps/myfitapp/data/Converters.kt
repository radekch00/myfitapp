package com.rachapps.myfitapp.data

import androidx.room.TypeConverter
import com.rachapps.myfitapp.data.enums.TrainingStatus
import java.time.LocalDate
import java.util.*

class Converters {
    @TypeConverter
    fun toDate(value: String): LocalDate? {
        return if(!value.isNullOrEmpty()){
            LocalDate.parse(value)
        }else{
            null
        }
    }

    @TypeConverter
    fun toDateString(date: LocalDate?): String? {
        return date?.toString()
    }

}