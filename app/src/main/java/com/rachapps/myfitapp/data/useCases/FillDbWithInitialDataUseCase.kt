package com.rachapps.myfitapp.data.useCases

import com.rachapps.myfitapp.data.PreferencesManager
import com.rachapps.myfitapp.data.remote.converters.BodyPartApiToEntity
import com.rachapps.myfitapp.data.remote.converters.ExerciseApiToEntity
import com.rachapps.myfitapp.data.repositories.ExerciseRepository
import com.rachapps.myfitapp.other.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async

class FillDbWithInitialDataUseCase(
    val scope: CoroutineScope,
    private val exerciseRepository: ExerciseRepository,
    private val preferencesManager: PreferencesManager
) {

    suspend fun execute(){
        getData(scope)
    }

    private suspend fun getData(scope : CoroutineScope) {

        var bodyParts = scope.async {
            exerciseRepository.getRemoteBodyParts()
        }

        var exercises = scope.async {
            exerciseRepository.getRemoteExercises()
        }
        val bodyPartsResponse = bodyParts.await()
        val exercisesResponse = exercises.await()

        if (bodyPartsResponse.status == Status.SUCCESS &&
            bodyPartsResponse.data != null &&
            exercisesResponse.status == Status.SUCCESS &&
            exercisesResponse.data != null
        ) {
            var exercises = exercisesResponse.data.exercises

            bodyPartsResponse.data.bodyParts.forEach { bodyPartResponse ->
                var bodyPartServerId = bodyPartResponse.id
                var bodyPart = BodyPartApiToEntity.convert(bodyPartResponse)
                var id = exerciseRepository.saveBodyPart(bodyPart)
                exercises.forEach {
                    if (it.bodyPartId == bodyPartServerId) {
                        exerciseRepository.saveExercise(
                            ExerciseApiToEntity.convert(
                                it,
                                id.toInt()
                            )
                        )
                    }
                }
            }
            preferencesManager.updateDataWasDownloaded(true)
        }
    }
}