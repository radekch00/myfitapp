package com.rachapps.myfitapp.data.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.time.LocalDateTime

@Entity(tableName = "setTable")
@Parcelize
data class SetEntity(
    val trainingId: Int,
    val exerciseId: Int,
    val order: Int,
    val weight: Double = 0.0,
    val repeats: Int = 0,
    val breakBefore: Int = 0,
    val warmUp: Boolean = false,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
) : Parcelable