package com.rachapps.myfitapp.data.remote

import com.rachapps.myfitapp.BuildConfig
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MyFitAPI {
    @GET("/api/bodyParts")
    suspend fun getBodyParts(
        @Query("API_KEY") apiKey : String = BuildConfig.API_KEY
    ) : Response<BodyPartsResponse>

    @GET("/api/exercises")
    suspend fun getExercises(
        @Query("API_KEY") apiKey : String = BuildConfig.API_KEY
    ) : Response<ExercisesResponse>

}