package com.rachapps.myfitapp.data.dto

import com.rachapps.myfitapp.data.entities.Training

data class WorkoutSummaryDTO(
    val training: Training,
    val setNo: Int = 0,
    val warmUpNo: Int = 0,
    val workingSetsNo: Int = 0,
    val totalWeight: Double = 0.0,
    val warmUpWeight: Double = 0.0,
    val workingSetsWeight: Double = 0.0,
    var records : List<ExerciseRecordDTO> = mutableListOf()
) {
}