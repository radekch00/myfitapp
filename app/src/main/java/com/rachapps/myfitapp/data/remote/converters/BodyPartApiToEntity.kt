package com.rachapps.myfitapp.data.remote.converters

import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.remote.responses.BodyPartResponse

object BodyPartApiToEntity {

    fun convert(bodyPartResponse: BodyPartResponse) : BodyPart{
        return BodyPart(bodyPartResponse.name)
    }
}