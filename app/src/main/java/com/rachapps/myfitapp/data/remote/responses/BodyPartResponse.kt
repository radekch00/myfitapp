package com.rachapps.myfitapp.data.remote.responses

data class BodyPartResponse (
    val name : String,
    val id : Int
)