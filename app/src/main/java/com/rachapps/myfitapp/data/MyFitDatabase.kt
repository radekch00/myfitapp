package com.rachapps.myfitapp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rachapps.myfitapp.data.dao.*
import com.rachapps.myfitapp.data.entities.*
import com.rachapps.myfitapp.data.entities.SetEntity

@Database(version = 1, entities = [Exercise::class, BodyPart::class, Training::class, SetEntity::class], exportSchema = false)
@TypeConverters(Converters::class)
abstract class MyFitDatabase : RoomDatabase(){
    abstract fun exerciseDao(): ExerciseDao
    abstract fun bodyPartDao(): BodyPartDao
    abstract fun trainingDao(): TrainingDao
    abstract fun setDao() : SetDao
}