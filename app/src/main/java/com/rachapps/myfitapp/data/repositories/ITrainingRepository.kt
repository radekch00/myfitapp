package com.rachapps.myfitapp.data.repositories

import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import com.rachapps.myfitapp.other.Resource
import kotlinx.coroutines.flow.Flow

interface ITrainingRepository {
    fun getAllExercisesWithSetNoByTrainingId(trainingId: Int?): Flow<List<ExerciseWithSetNo>>
    fun getSetAndExercise(
        trainingId: Int? = null,
        setId: Int? = null,
        limit : Int? = null,
        order: Int? = null
    ): Flow<List<SetAndExercise>>

    fun getSetsByTrainingId(trainingId: Int): Flow<List<SetEntity>>
    fun getSetAndExerciseByTrainingId(trainingId: Int, limit : Int, order : Int): Flow<List<SetAndExercise>>
    fun getSetsAndExercisesById(setId: Int, order: Int): Flow<List<SetAndExercise>>

    suspend fun insertTraining(training: Training): Long

    suspend fun updateTraining(training: Training)

    suspend fun saveTraining(training: Training): Training

    suspend fun insertSet(set: SetEntity): Long

    suspend fun updateSets(sets: MutableList<SetEntity>)

    suspend fun saveSet(set: SetEntity): SetEntity

    suspend fun updateSet(set: SetEntity)

    suspend fun deleteSet(set: SetEntity)

    suspend fun deleteLastSetByTrainingIDAndExerciseId(trainingId: Int, exerciseId: Int)

    suspend fun selectMaxWeight(exerciseId : Int, trainingId: Int? = null, notTrainingId : Int? = null) : SetAndExercise

    suspend fun countSetsByExerciseId(exerciseId: Int) : Int
}