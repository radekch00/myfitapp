package com.rachapps.myfitapp.data.useCases

import android.util.Log
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.repositories.TrainingRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class SortSetsUseCase constructor(
    private val trainingRepository: TrainingRepository,
    private val scope: CoroutineScope,
    private val listener: OnOrderChanged
) {

    private val TAG = "SortSetsUseCase"


    fun execute(trainingId: Int, from: Int, to: Int) {
        var t = if(to < 0) 0 else to
        scope.launch {
            trainingRepository.getSetsByTrainingId(trainingId).collect { setList ->
                this.coroutineContext.job.cancel()

                var list = setList.toMutableList()
                list.add(t, list.removeAt(from))

                var newList = mutableListOf<SetEntity>()

                var newOrder = 0
                list.forEach {
                    newList.add(it.copy(order = newOrder))
                    newOrder++
                }
                scope.launch {
                    runBlocking {
                        for (set in newList) {
                            Log.d(TAG, "update: $set")
                            trainingRepository.updateSet(set)
                        }
                        listener.onOrderChanged()
                    }

                }
            }
        }
    }

    interface OnOrderChanged {
        fun onOrderChanged()
    }
}