package com.rachapps.myfitapp.data.entities.relations

import android.os.Parcelable
import androidx.room.Embedded
import com.rachapps.myfitapp.data.entities.Exercise
import kotlinx.parcelize.Parcelize

@Parcelize
data class ExerciseWithSetNo constructor(
    @Embedded
    val exercise: Exercise? = null,
    var setNo: Int = 0
) : Parcelable {
}