package com.rachapps.myfitapp.data.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "bodyPart")
@Parcelize
data class BodyPart(
    val name : String,
    @PrimaryKey(autoGenerate = true) val id : Int = 0
) : Parcelable
{
}