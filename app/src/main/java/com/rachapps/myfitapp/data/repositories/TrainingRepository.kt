package com.rachapps.myfitapp.data.repositories

import com.rachapps.myfitapp.data.dao.ExerciseDao
import com.rachapps.myfitapp.data.dao.SetDao
import com.rachapps.myfitapp.data.dao.TrainingDao
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.remote.MyFitAPI
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import com.rachapps.myfitapp.other.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import retrofit2.Response
import javax.inject.Inject

class TrainingRepository @Inject constructor(
    private val trainingDao: TrainingDao,
    private val exerciseDao: ExerciseDao,
    private val setDao: SetDao
) : ITrainingRepository {

    override fun getAllExercisesWithSetNoByTrainingId(trainingId: Int?) =
        exerciseDao.getAllExercisesWithSetNo(trainingId)

    override fun getSetAndExercise(
        trainingId: Int?,
        setId: Int?,
        limit : Int?,
        order: Int?
    ): Flow<List<SetAndExercise>> {
        return if (trainingId != null && setId == null) {
            getSetAndExerciseByTrainingId(trainingId = trainingId,order = order ?: 0, limit = limit ?:1000)
        } else if (trainingId != null && setId != null) {
            getSetsAndExercisesById(setId, order ?: 0)

        } else {
            MutableStateFlow(listOf<SetAndExercise>())
        }
    }

    override fun getSetsByTrainingId(trainingId: Int) = setDao.selectSetByTrainingId(trainingId)

    override fun getSetAndExerciseByTrainingId(trainingId: Int, limit : Int, order : Int) =
        setDao.selectSetAndExerciseByTrainingId(trainingId, limit, order)

    override fun getSetsAndExercisesById(setId: Int, order: Int) =
        setDao.selectSetAndExerciseBySetId(setId, order)

    override suspend fun insertTraining(training: Training) = trainingDao.insert(training)

    override suspend fun updateTraining(training: Training) = trainingDao.update(training)

    override suspend fun saveTraining(training: Training): Training {
        return if (training.id == 0) {
            val trainingId = insertTraining(training)
            training.copy(id = trainingId.toInt())
        } else {
            updateTraining(training)
            training
        }

    }

    override suspend fun insertSet(set: SetEntity) = setDao.insert(set)

    override suspend fun updateSets(sets: MutableList<SetEntity>) =
        setDao.updateSets(*sets.toTypedArray())

    override suspend fun saveSet(set: SetEntity): SetEntity {
        return if (set.id == 0) {
            val setId = insertSet(set)
            set.copy(id = setId.toInt())
        } else {
            updateSet(set)
            set
        }
    }

    override suspend fun updateSet(set: SetEntity) = setDao.update(set)

    override suspend fun deleteSet(set: SetEntity) = setDao.delete(set)

    override suspend fun deleteLastSetByTrainingIDAndExerciseId(trainingId: Int, exerciseId: Int) =
        setDao.deleteLastSetByTrainingIDAndExerciseId(trainingId, exerciseId)


    override suspend fun selectMaxWeight(exerciseId : Int, trainingId: Int?, notTrainingId : Int?) : SetAndExercise{
        return if(trainingId == null && notTrainingId == null){
            setDao.selectMaxWeightByExerciseId(exerciseId)
        }else if(trainingId != null){
            setDao.selectMaxWeightByExerciseIdAndTrainingId(exerciseId, trainingId)
        }else {
            setDao.selectMaxWeightByExerciseIdAndNotTrainingId(exerciseId, notTrainingId!!)
        }
    }

    override suspend fun countSetsByExerciseId(exerciseId: Int): Int {
        return setDao.countSetsByExerciseId(exerciseId)
    }
}


