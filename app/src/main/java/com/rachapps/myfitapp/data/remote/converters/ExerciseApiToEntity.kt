package com.rachapps.myfitapp.data.remote.converters

import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.remote.responses.ExerciseResponse

object ExerciseApiToEntity {

    public fun convert(exerciseResponse: ExerciseResponse, bodyPartId : Int = 0) : Exercise{
        return Exercise(exerciseResponse.name, if(bodyPartId != 0)  bodyPartId else exerciseResponse.bodyPartId, 0)
    }
}