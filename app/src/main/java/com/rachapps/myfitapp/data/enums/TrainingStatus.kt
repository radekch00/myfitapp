package com.rachapps.myfitapp.data.enums

enum class TrainingStatus() {
    READY,
    IN_PROGRESS,
    DONE
}