package com.rachapps.myfitapp.ui.workout

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.rachapps.myfitapp.MainDispatcherRule
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.repositories.FakeTrainingRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.google.common.truth.Truth.assertThat

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Boolean.TRUE
import java.lang.reflect.Field

class WorkoutExerciseViewModelTest {

    private lateinit var viewModel: WorkoutExerciseViewModel
    private val setAndExerciseEmptyList = mutableListOf<SetAndExercise>()
    private val setAndExerciseList = mutableListOf<SetAndExercise>()

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val savedStateHandle = SavedStateHandle()
        savedStateHandle["training"] = Training(null, "title", id = 1)
        viewModel = WorkoutExerciseViewModel(savedStateHandle, FakeTrainingRepository())
        setAndExerciseList.add(SetAndExercise(SetEntity(1, 1, 1), Exercise("Exercise1", 1, 1)))
        setAndExerciseList.add(SetAndExercise(SetEntity(1, 2, 2), Exercise("Exercise2", 1, 1)))
    }

    @Test
    fun `emptyList`() {
        val method = viewModel.javaClass.getDeclaredMethod(
            "setUpCurrentAndNextSetAndExercise",
            MutableList::class.java
        )
        method.isAccessible = true
        val parameters = arrayOfNulls<Any>(1)
        parameters[0] = setAndExerciseEmptyList
        method.invoke(viewModel, *parameters)

        val currentSet = getFieldValue("currentSet")
        val currentExercise = getFieldValue("currentExercise")
        val nextSet = getFieldValue("nextSet")
        val nextExercise = getFieldValue("nextExercise")

        assertThat(currentSet).isNull()
        assertThat(currentExercise).isNull()
        assertThat(nextSet).isNull()
        assertThat(nextExercise).isNull()

    }

    @Test
    fun `notEmptyList`() {
        val method = viewModel.javaClass.getDeclaredMethod(
            "setUpCurrentAndNextSetAndExercise",
            MutableList::class.java
        )
        method.isAccessible = true
        val parameters = arrayOfNulls<Any>(1)
        parameters[0] = setAndExerciseList
        method.invoke(viewModel, *parameters)

        val currentSet = getFieldValue("currentSet")
        val currentExercise = getFieldValue("currentExercise")
        val nextSet = getFieldValue("nextSet")
        val nextExercise = getFieldValue("nextExercise")

        assertThat(currentSet).isEqualTo(setAndExerciseList[0].set)
        assertThat(currentExercise).isEqualTo(setAndExerciseList[0].exercise)
        assertThat(nextSet).isEqualTo(setAndExerciseList[1].set)
        assertThat(nextExercise).isEqualTo(setAndExerciseList[1].exercise)
    }

    private fun getFieldValue(fieldName : String) : Any?{
        val nextExerciseField = viewModel.javaClass.getDeclaredField(fieldName)
        nextExerciseField.isAccessible = true
        return nextExerciseField.get(viewModel)
    }


}