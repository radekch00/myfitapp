package com.rachapps.myfitapp.data.repositories

import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.entities.SetEntity
import com.rachapps.myfitapp.data.entities.Training
import com.rachapps.myfitapp.data.entities.relations.ExerciseWithSetNo
import com.rachapps.myfitapp.data.entities.relations.SetAndExercise
import com.rachapps.myfitapp.data.remote.responses.BodyPartResponse
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import com.rachapps.myfitapp.other.Resource
import com.rachapps.myfitapp.other.Status
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

class FakeTrainingRepository : ITrainingRepository {
    private val bodyPartList = mutableListOf<BodyPart>()
    private val observableListOfBodyParts = MutableStateFlow(bodyPartList)

    private val exercisesWithSetNoList = mutableListOf<ExerciseWithSetNo>()
    private val observableListOfExerciseWithSetNo = MutableStateFlow(exercisesWithSetNoList)

    private val setAndExerciseList = mutableListOf(SetAndExercise(SetEntity(1,1,1),
        Exercise("name",1,1)
    ),
        SetAndExercise(SetEntity(1,1,2),Exercise("name",1,1)))
    private val observableListOfSetAndExercise = MutableStateFlow(setAndExerciseList)

    private val setList = mutableListOf<SetEntity>()
    private val observableListOfSet = MutableStateFlow(setList)

    private val trainingList = mutableListOf<Training>()
    private val observableListOfTraining = MutableStateFlow(trainingList)

    private var shouldReturnNetworkError = false

    fun setShouldReturnNetworkError(value: Boolean) {
        shouldReturnNetworkError = value
    }

    private var sequence: Long = 1

    private fun getNextSequence(): Long {
        sequence++
        return sequence
    }


    override fun getAllExercisesWithSetNoByTrainingId(trainingId: Int?): Flow<List<ExerciseWithSetNo>> {
        return observableListOfExerciseWithSetNo
    }

    override fun getSetAndExercise(
        trainingId: Int?,
        setId: Int?,
        limit: Int?,
        order: Int?
    ): Flow<List<SetAndExercise>> {
        return observableListOfSetAndExercise
    }

    override fun getSetsByTrainingId(trainingId: Int): Flow<List<SetEntity>> {
        return observableListOfSet
    }

    override fun getSetAndExerciseByTrainingId(
        trainingId: Int,
        limit: Int,
        order: Int
    ): Flow<List<SetAndExercise>> {
        return observableListOfSetAndExercise
    }

    override fun getSetsAndExercisesById(setId: Int, order: Int): Flow<List<SetAndExercise>> {
        return observableListOfSetAndExercise
    }

    override suspend fun insertTraining(training: Training): Long {
        val id = getNextSequence()
        val newTraining = training.copy(id = id.toInt())
        trainingList.add(newTraining)
        return id
    }

    override suspend fun updateTraining(training: Training) {
        val iterator = trainingList.listIterator()
        while (iterator.hasNext()) {
            val trainingFromList = iterator.next()
            if (trainingFromList.id == training.id) {
                iterator.set(training)
            }
        }
    }

    override suspend fun saveTraining(training: Training): Training {
        return if (training.id == 0) {
            val id = insertTraining(training)
            training.copy(id = id.toInt())
        } else {
            updateTraining(training)
            training
        }
    }

    override suspend fun insertSet(set: SetEntity): Long {
        val id = getNextSequence()
        setList.add(set.copy(id = id.toInt()))
        return id
    }

    override suspend fun updateSets(sets: MutableList<SetEntity>) {
        sets.forEach {
            updateSet(it)
        }
    }

    override suspend fun saveSet(set: SetEntity): SetEntity {
        return if(set.id == 0){
            val id = getNextSequence()
            set.copy(id = id.toInt())
        }else{
            updateSet(set)
            set
        }
    }

    override suspend fun updateSet(set: SetEntity) {
        val iterator = setList.listIterator()
        while (iterator.hasNext()) {
            val setFromList = iterator.next()
            if (setFromList.id == set.id) iterator.set(set)
        }
    }

    override suspend fun deleteSet(set: SetEntity) {
        setList.remove(set)
    }

    override suspend fun deleteLastSetByTrainingIDAndExerciseId(trainingId: Int, exerciseId: Int) {
        val size = setList.size
        var position = size - 1
        val iterator = setList.listIterator()
        while(iterator.hasPrevious()){
            val set = iterator.previous()
            if(set.exerciseId == exerciseId && set.trainingId == trainingId){
                setList.removeAt(position)
            }
            position--
        }
    }

    override suspend fun selectMaxWeight(
        exerciseId: Int,
        trainingId: Int?,
        notTrainingId: Int?
    ): SetAndExercise {
        return SetAndExercise(SetEntity(1,1,1), Exercise("name",1))
    }
}