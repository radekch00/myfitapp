package com.rachapps.myfitapp.data.repositories

import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.remote.responses.BodyPartResponse
import com.rachapps.myfitapp.data.remote.responses.BodyPartsResponse
import com.rachapps.myfitapp.data.remote.responses.ExercisesResponse
import com.rachapps.myfitapp.other.Resource

class FakeExerciseRepository : IExerciseRepository {
    override suspend fun saveExercise(exercise: Exercise): Long {
        TODO("Not yet implemented")
    }

    override suspend fun saveBodyPart(bodyPart: BodyPart): Long {
        TODO("Not yet implemented")
    }

    override suspend fun getRemoteBodyParts(): Resource<BodyPartsResponse> {
        return Resource.success(BodyPartsResponse(listOf<BodyPartResponse>(),1,"success"))
    }

    override suspend fun getRemoteExercises(): Resource<ExercisesResponse> {
        return Resource.success(ExercisesResponse(listOf(),1,"success"))
    }
}