package com.rachapps.myfitapp.data.remote.converters

import com.google.common.truth.Truth.assertThat
import com.rachapps.myfitapp.data.entities.BodyPart
import com.rachapps.myfitapp.data.remote.responses.BodyPartResponse
import org.junit.Before
import org.junit.Test


class BodyPartApiToEntityTest{

    private lateinit var bodyPartResponse : BodyPartResponse
    private lateinit var bodyPartApiToEntity : BodyPartApiToEntity

    @Before
    fun setUp(){
        bodyPartResponse = BodyPartResponse("name", 1)
    }

    @Test
    fun convert(){
        val entity = BodyPartApiToEntity.convert(bodyPartResponse)
        assertThat(entity).isEqualTo(BodyPart("name",0))
    }
}