package com.rachapps.myfitapp.data.remote.converters

import com.google.common.truth.Truth.assertThat
import com.rachapps.myfitapp.data.entities.Exercise
import com.rachapps.myfitapp.data.remote.responses.ExerciseResponse

import org.junit.Before
import org.junit.Test

class ExerciseApiToEntityTest{

    lateinit var exercise: Exercise

    @Before
    fun setUp(){
        exercise = Exercise("name",1,0)
    }

    @Test
    fun convertWithoutBodyPartId(){
        val exerciseResponse = ExerciseResponse("name",1,1)
        val answer = ExerciseApiToEntity.convert(exerciseResponse)
        assertThat(answer).isEqualTo(Exercise("name",1,0))
    }

    @Test
    fun convertWitBodyPartId(){
        val exerciseResponse = ExerciseResponse("name",1,1)
        val answer = ExerciseApiToEntity.convert(exerciseResponse,3)
        assertThat(answer).isEqualTo(Exercise("name",3,0))
    }
}

